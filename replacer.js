var glob = require('glob'),
	fs = require('fs-extra'),
	path = require('path');

var replacers = [
	// [new RegExp("require\\('be_konstabel'\\)", "g"), "require('engine.be_konstabel')"],
	// [new RegExp("require\\('be_transmanager'\\)", "g"), "require('engine.be_transmanager')"],
	// [new RegExp("require\\('be_saveloader'\\)", "g"), "require('engine.be_saveloader')"],
	// [new RegExp("require\\('be_scene'\\)", "g"), "require('engine.be_scene')"],
	// [new RegExp("require\\('be_state'\\)", "g"), "require('engine.be_state')"],
	// [new RegExp("require\\('be_uiel'\\)", "g"), "require('engine.be_uiel')"],
	// [new RegExp("require\\('be_stacklog'\\)", "g"), "require('engine.be_stacklog')"],
	// [new RegExp("require 'be_stacklog'", "g"), "require('engine.be_stacklog')"],
	// [new RegExp("require\\('bidons'\\)", "g"), "require('engine.bidons')"],
	// [new RegExp("require\\('binser'\\)", "g"), "require('engine.binser')"],
	// [new RegExp("require\\('cargo'\\)", "g"), "require('engine.cargo')"],
	// [new RegExp("require\\('fastdraw'\\)", "g"), "require('engine.fastdraw')"],
	// [new RegExp("require\\('flux'\\)", "g"), "require('engine.flux')"],
	// [new RegExp("require\\('gamestate'\\)", "g"), "require('engine.gamestate')"],
	// [new RegExp("require\\('inspect'\\)", "g"), "require('engine.inspect')"],
	// [new RegExp("require\\('json'\\)", "g"), "require('engine.json')"],
	// [new RegExp("require\\('listbox'\\)", "g"), "require('engine.listbox')"],
	// [new RegExp("require\\('log'\\)", "g"), "require('engine.log')"],
	// [new RegExp("require\\('lume'\\)", "g"), "require('engine.lume')"],
	// [new RegExp("require\\('middleclass'\\)", "g"), "require('engine.middleclass')"],
	// [new RegExp("require\\('require'\\)", "g"), "require('engine.require')"],
	// [new RegExp("require\\('scenemanager'\\)", "g"), "require('engine.scenemanager')"],
	// [new RegExp("require\\('splashy'\\)", "g"), "require('engine.splashy')"],
	// [new RegExp("require\\('splashy'\\)", "g"), "require('engine.splashy')"],
	// [new RegExp("require\\('timer'\\)", "g"), "require('engine.timer')"],
	// [new RegExp("require\\('tween'\\)", "g"), "require('engine.tween')"],
	// [new RegExp("require\\('uisloader'\\)", "g"), "require('engine.uisloader')"],
	// [new RegExp("require\\('utils'\\)", "g"), "require('engine.utils')"],
	// [new RegExp("require\\('csvparser'\\)", "g"), "require('engine.csvparser')"],
	// [new RegExp("require 'extend_string'", "g"), "require('engine.extend_string')"],
	// [new RegExp("require 'lualinq'", "g"), "require('engine.lualinq')"],
	// [new RegExp("require\\('ut-methods'\\)", "g"), "require('engine.ut-methods')"],

	[new RegExp("require\\('ge_context'\\)", "g"), "require('classes.ge_context')"],
	[new RegExp("require\\('ge_chapter'\\)", "g"), "require('classes.ge_chapter')"],
	[new RegExp("require\\('ge_char'\\)", "g"), "require('classes.ge_char')"],
	[new RegExp("require\\('ge_item'\\)", "g"), "require('classes.ge_item')"],
	[new RegExp("require\\('ge_player'\\)", "g"), "require('classes.ge_player')"],

	// [new RegExp("require\\('uis.ui_container'\\)", "g"), "require('engine.uis.ui_container')"],
	// [new RegExp("require 'elements'", "g"), "require 'engine.elements'"],
	// [new RegExp("require\\('elements\.", "g"), "require('engine.elements."],
	// [new RegExp("require 'elements\.", "g"), "require 'engine.elements."],
	// [new RegExp("require 'behaviors\.", "g"), "require 'engine.behaviors."],
	// [new RegExp("require\\('behaviors\.", "g"), "require('engine.behaviors."]
];

function mkdirp(filepath) {
    var dirname = path.dirname(filepath);

    if (!fs.existsSync(dirname)) {
        mkdirp(dirname);
    }

    fs.mkdirSync(dirname);
}

glob('**/*.lua', { cwd: 'distr/game' }, function(er, files) {
	files.forEach(file => {
		var full_text = fs.readFileSync('distr/game/' + file, 'utf-8');
		replacers.forEach(replacer => {
			full_text = full_text.replace(replacer[0], replacer[1]);
		});
		fs.ensureDirSync('distr/game/' + path.dirname(file));
		fs.writeFileSync('distr/game/' + file, full_text);
		console.log(`${file} done`)
	});
}); 


