const exec = require('gulp-exec'),
	rename = require('gulp-rename'),
	replace = require('gulp-replace'),
	watch = require('gulp-watch'),
	batch = require('gulp-batch'),
	gulp = require('gulp')

gulp.task('build', function() {
	return gulp
	.src('**/*.moon')
	.pipe(watch('**/*.moon'))
	.pipe(exec('moonc -p "<%= file.path %>"', { pipeStdout: true }))
	.pipe(replace('  ', '\t'))
	.pipe(rename(function(path) {
		path.extname = '.lua'
	}))
	.pipe(gulp.dest('.'))
})