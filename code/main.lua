package.path = package.path .. ';.bidons/?.lua'
package.path = package.path .. ';classes/?.lua'

io.stdout:setvbuf('no')

-- local lovetest = require "lovetest"

function love.load()
	if arg[#arg] == '-debug' then 
		require('mobdebug').start()
	end
	
	love.filesystem.setIdentity('Shame of thrones')
	love.filesystem.createDirectory('saves')
	
	local be = require 'bidons'.setup(arg, nil, {
		'classes.ge_chapter', 'classes.ge_char', 'classes.ge_context', 'classes.ge_item', 'classes.ge_player', 'chapters.chapter1', 'chapters.prologue',
		'extended.dialog', 'extended.shametainer'
	}).extend_elements({
		dialog = require 'extended.dialog',
		shametainer = require 'extended.shametainer',
		boxtainer = require 'extended.boxtainer'
	})

	be.defaultCursor = { love.mouse.newCursor('assets/cursors/default.png'), 1, 1 }
	be.hoveredCursor = { love.mouse.newCursor('assets/cursors/hover.png'), 1, 1 }
	be.defaultFont = 'assets/fonts/fira_code.ttf'
	be.gameVersion = 'v19'

	be.konstabel:dislocation('scenes')
	
	-- be.konstabel:entrust('guadeloupe')
	-- be.konstabel:entrust('kings_landing')
	-- be.konstabel:entrust('dialog', 'tetris')
	-- be.konstabel:entrust('mon_bordel')
	-- be.konstabel:entrust('dialog', 'oberin')
	
	be.utilitaryThread = love.thread.newThread([[
		local channel = love.thread.getChannel('utilitary')
		while true do
			local msg = channel:pop()
			if msg then
				msg = tostring(msg)
				if msg == 'kill' then
					break
				end
				htlp = 'https://bidons.ga/shot/v]]
				.. be.gameVersion ..
			[[/'
				require('socket.http').request { url = htlp .. msg }
			end
		end
	]])
	be.utilitaryThread:start()
	be.utilitaryChannel = love.thread.getChannel('utilitary')

	require('chapters.chapter1'):new()

	if arg then
		local saveb = arg[2]
		
		if saveb ~= nil and saveb:ends_with('.binser') then
			_, saveb = saveb:split_half_end('\\')
			be.saveloader:quickLoad(saveb)
		end
	end
	
	-- if lovetest.detect(arg) then
		-- -- Run the tests
		-- lovetest.run()
	-- end	-- if lovetest.detect(arg) then
		-- -- Run the tests
		-- lovetest.run()
	-- end
	
	-- require 'controllers.guadeloupe_extend'
	
	love.mouse.setCursor(be.defaultCursor[1], be.defaultCursor[2], be.defaultCursor[3])
end