function love.conf(t)
    t.window.width = 1280
    t.window.height = 720
	t.title = 'Shame of thrones'
	t.window.borderless = false
	t.window.centered = true
	t.window.y = 0
	t.window.x = 0
end