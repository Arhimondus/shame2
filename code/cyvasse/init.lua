local be = require 'bidons'
local fd = require 'fastdraw'

local lume = require 'lume'
local utils = require 'utils'
local flux = require 'flux'

local Cell = require 'cyvasse.classes.cell'
local Player = require 'cyvasse.classes.player'
local PlacedCell = require 'cyvasse.classes.placed_cell'
local Unit = require 'cyvasse.classes.unit'
local TextButton = require 'cyvasse.classes.text_button'

local Ai = require 'cyvasse.classes.ai'
local Tupoy = require 'cyvasse.classes.ai_tupoy'
local Volt = require 'cyvasse.classes.ai_volt'

local assets = require('cargo').init('assets')

local big_font = assets.fonts.game_of_thrones_kg(30)
local hovered_cell = nil
local holded_cell = nil
local moving_cell = nil

local cyvasse = {}

function cyvasse:enter()
	local start_x = (love.graphics.getWidth() - Cell.static.width * 11) / 2
	local start_y = love.graphics.getHeight() / 2 - Cell.static.height / 2
	
	be.cyvasse = { phase = 'preflop', cells = {}, units = {}, units_pool = { 'crossbowman', 'crossbowman', 'rabble', 'rabble', 'spearman', 'spearman', 'lighthorse', 'lighthorse', 'heavyhorse', 'heavyhorse', 'elephant', 'elephant', 'king', 'catapult', 'trebuchet', 'dragon', 'mountain', 'mountain' }, controls = {}, turn = 1, players = {} }
	
	for j = 1, 11 do
		Cell:new(start_x + (j - 1) * Cell.static.width, start_y, 'F' .. j, 'mid')
	end
	
	local rows = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L' }
	
	for i = 1, 5 do
		for j = 1, 11 - i do
			Cell:new(start_x + (Cell.static.width / 2) * i + (j - 1) * Cell.static.width, start_y + Cell.static.height * 0 + i * (Cell.static.height * 1.5), rows[6 - i] .. j, 'bot', i)
		end
		for j = 1, 11 - i do
			Cell:new(start_x + (Cell.static.width / 2) * i + (j - 1) * Cell.static.width, start_y - Cell.static.height * 0 - i * (Cell.static.height * 1.5), rows[6 + i] .. j, 'top', i)
		end
	end

	for i, v in ipairs(be.cyvasse.cells) do
		for j, b in ipairs(be.cyvasse.cells) do
			if v ~= b then
				if utils.rects_intersect(v.rect, b.rect) then
					table.insert(v.pathes, b)
				end
			end
		end
	end
	
	local player = Player:new('#b2b5ff', 'bot')
	local enemy = Player:new('#ed3d3e', 'top')
	
	enemy:setAi(Volt:new())
	
	function start_game()
		Unit.static.randomPlacement(player)
		
		be.cyvasse.phase = 'flop'
		table.remove(be.cyvasse.controls, 1)
		table.remove(be.cyvasse.controls, 1)
		table.remove(be.cyvasse.controls, 1)
		
		enemy.ai:setStartingPosition()
		-- Unit.static.randomPlacement(enemy)
		
		for i, v in lume.ripairs(be.cyvasse.cells) do
			if v.class.name == 'PlacedCell' then
				if v.unit then
					local index = Unit.static.findIndex(v.unit)
					table.remove(be.cyvasse.units, index)
				end
				local index = Cell.static.findIndex(v)
				table.remove(be.cyvasse.cells, index)
			end
		end
		
		local ai_king_row = math.abs(Ai.findFirstUnit(self, 'king', enemy).cell.row)
		local player_king_row = math.abs(Ai.findFirstUnit(self, 'king', player).cell.row)
		if ai_king_row < player_king_row then
			Player:nextTurn()
		end
		
		save_placement()
	end
	
	function load_placement(placement)
		if placement == nil then
			placement = be.context.cyvasse.last_placement
		end
		
		if placement == nil then
			return
		end
		
		fr(be.cyvasse.cells):where(function(c) return c.unit and c.unit.player == player end):foreach(function(c)
			local index = Unit.static.findIndex(c.unit)
			table.remove(be.cyvasse.units, index)
			c.unit = nil
		end)
	
		fr(placement):foreach(function(p)
			Unit(p.unit, player, Cell.static.findById(p.cell))
		end)

		fr(be.cyvasse.cells)
			:where(function(e) return e.class.name == 'PlacedCell' and e.owner == player end)
			:foreach(function(e)
				if e.unit then
					local index = Unit.static.findIndex(e.unit)
					table.remove(be.cyvasse.units, index)
				end
				local index = Cell.static.findIndex(e)
				table.remove(be.cyvasse.cells, index)
			end)
	end

	function save_placement()
		local playerPlacement = fr(be.cyvasse.cells)
			:where(function(e) return e.unit and e.unit.player == player end)
			:select(function(e) return { cell = e.id, unit = e.unit.type } end)
			:toArray()
		be.context.cyvasse.last_placement = playerPlacement
	end

	TextButton:new(800, 575, be.transmanager:get('cyvasse_loadlastplacement'), '#ffffffff', '#7f6b3e', load_placement)
	TextButton:new(192, 545, be.transmanager:get('cyvasse_startgame'), '#ffffffff', '#7f6b3e', start_game)

	TextButton:new(192, 575, be.transmanager:get('cyvasse_randomplacement'), '#ffffffff', '#7f6b3e', function()
		Unit.static.randomPlacement(player)
		start_game()
	end)
end

local cyvasse_victory = be.transmanager:get('cyvasse_victory')
local cyvasse_defeat = be.transmanager:get('cyvasse_defeat')

function cyvasse.draw()
	fd.color()

	love.graphics.draw(assets.cyvasse.background, 0, 0)
	
	for i, v in ipairs(be.cyvasse.cells) do
		v:draw()
	end
	for i, v in ipairs(be.cyvasse.units) do
		v:draw()
	end
	if hovered_cell then
		fd.color().font().printd(hovered_cell.id .. ' ' .. hovered_cell.row, 0, 0)
	end
	
--	fd.color().font().print('FPS ' .. love.timer.getFPS(), 0, 10)
--	fd.color().font().print(be.cyvasse.phase, 0, 20)
	
	for i, v in ipairs(be.cyvasse.controls) do
		v:draw()
	end
	
	if be.cyvasse.phase == 'reaver' then
		fd.color('#00000088').rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
		if be.cyvasse.result == 'victory' then
			fd.color().font(big_font).printd(cyvasse_victory, (love.graphics.getWidth() - big_font:getWidth(cyvasse_victory)) / 2, (love.graphics.getHeight() - big_font:getHeight()) / 2)
		elseif be.cyvasse.result == 'defeat' then
			fd.color().font(big_font).printd(cyvasse_defeat, (love.graphics.getWidth() - big_font:getWidth(cyvasse_defeat)) / 2, (love.graphics.getHeight() - big_font:getHeight()) / 2)
		end
	end
	
	if holded_cell and holded_cell.unit and holded_cell.unit.dragging then
		if be.cyvasse.phase ~= 'preflop' then
			local x1, y1 = holded_cell.x + Cell.width / 2, holded_cell.unit.y + Cell.height
			local x2, y2 = love.mouse.getPosition()
			-- love.graphics.setLineWidth(10)
			drawLine(x1, y1, x2, y2)
			-- love.graphics.setLineWidth(1)
		end
	end
	
	-- fd.color().font().printd(zz, 100, 100)
end

zz = 'ZERO'

function drawLine(x1, y1, x2, y2)
  love.graphics.setPointSize(5)

  local x, y = x2 - x1, y2 - y1
  local len = math.sqrt(x^2 + y^2)
  local stepx, stepy = x / len * 10, y / len * 10
  x = x1
  y = y1

  for i = 1, len / 10 do
    love.graphics.points(x, y)
    x = x + stepx
    y = y + stepy
  end
end

function cyvasse:mousemoved(x, y, dx, dy)
	if be.cyvasse.players[be.cyvasse.turn].type ~= 'player' then return end
	
	if holded_cell and holded_cell.unit and holded_cell.unit.dragging then
		if be.cyvasse.phase == 'preflop' then
			holded_cell.unit.x = holded_cell.unit.x + dx
			holded_cell.unit.y = holded_cell.unit.y + dy
		end
		return
	end
	
	for i, v in ipairs(be.cyvasse.controls) do
		v:mousemoved(x, y)
	end
	
	for i, v in lume.ripairs(be.cyvasse.cells) do
		local mm = v:mousecheck(x, y)
		if mm ~= nil then
			if hovered_cell == v then
				return
			end
			if hovered_cell ~= nil then
				hovered_cell:deselect()
			end
			hovered_cell = mm
--			hovered_cell:select()
			return
		else
			if hovered_cell ~= nil and hovered_cell == v then
--				hovered_cell:deselect()
				hovered_cell = nil
			end
		end
	end
end

function cyvasse:mousepressed(x, y)
	if be.cyvasse.phase == 'reaver' then
		if be.cyvasse.result == 'victory' then
			be.context.cyvasse.result = 'victory'
			-- be.context.chars.Player.authority = be.context.chars.Player.authority + 5
		else
			be.context.cyvasse.result = 'defeat'
			-- be.context.chars.Player.authority = be.context.chars.Player.authority - 5 
		end
		be.konstabel:entrust(be.context.cyvasse.backstate[1], be.context.cyvasse.backstate[2])
	end
	if be.cyvasse.players[be.cyvasse.turn].type ~= 'player' then return end
	
	if not hovered_cell or not hovered_cell.unit then
		love.mousemoved(x, y, 0, 0)
	end
	if hovered_cell and hovered_cell.unit then -- and hovered_cell.class.name == 'PlacedCell'
		holded_cell = hovered_cell
		holded_cell.unit:startDragging()
	end
	
	for i, v in ipairs(be.cyvasse.controls) do
		v:mousepressed(x, y)
	end
end

function cyvasse:mousereleased(x, y)
	if holded_cell and holded_cell.unit then
		holded_cell.unit:stopDragging(x, y)
		holded_cell = nil
	end
end

function cyvasse:keypressed(key)
	if be.cyvasse.players[be.cyvasse.turn].type ~= 'player' then return end
	
	if be.cyvasse.moving_cell then return end
end

function cyvasse:update(dt)
	flux.update(dt)
	if be.cyvasse.moving_cell then be.cyvasse.moving_cell:update(dt) end
end

return cyvasse