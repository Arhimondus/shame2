local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local fd = require 'fastdraw'
local utils = require 'utils'

local TextButton = class('TextButton')

function TextButton:initialize(x, y, text, color, hcolor, onclick)
	self.x = x
	self.y = y
	self.text = text
	self.color = color
	self.hcolor = hcolor
	self.onclick = onclick
	self.font = assets.fonts.game_of_thrones_kg(16)
	self.width = self.font:getWidth(self.text)
	self.height = self.font:getHeight()
	self.hovered = false
	table.insert(be.cyvasse.controls, self)
end

function TextButton:draw()
	fd.color(self.color).printd(text, x, y)
end

function TextButton:mousemoved(x, y)
	if utils.inside_rectangle(x, y, self.x, self.y, self.width, self.height) then
		self.hovered = true
	else
		self.hovered = false
	end
end

function TextButton:mousepressed(x, y)
	if self.hovered then
		self.onclick()
	end
end

function TextButton:draw()
	fd.color(self.hovered and self.hcolor or self.color).font(self.font).printd(self.text, self.x, self.y)
end

return TextButton