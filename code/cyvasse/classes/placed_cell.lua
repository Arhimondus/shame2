local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local fd = require 'fastdraw'
local Cell = require 'cyvasse.classes.cell'
local Unit = require 'cyvasse.classes.unit'

local PlacedCell = class('PlacedCell', Cell)

function PlacedCell:initialize(x, y, unit_type, player, id)
	self.owner = player
	Cell.initialize(self, x, y, id)
	Unit:new(unit_type, player, self)
end

function PlacedCell:draw()
	fd.color(not self.unit and '#ffffff' or self.unit.player.color).pol6x('line', self.flatPoly)
	.pol6x('fill', self.flatPoly)
	
	local color
	if self.hovered then
		color = '#ff0000'
	elseif self.pathable then
		color = '#ffcbdb'
	else
		color = '#000000'
	end
	
	fd.color(color).pol6x('line', self.flatPolyInner)
	.pol6x('fill', self.flatPolyInner)
	
--	if self.unit then
--		self.unit:draw()
--	end
end

return PlacedCell