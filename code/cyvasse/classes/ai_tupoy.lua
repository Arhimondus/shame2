local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local Ai = require 'cyvasse.classes.ai'
local Unit = require 'cyvasse.classes.unit'

local Tupoy = class('Tupoy', Ai)

function Tupoy:initialize()
	Ai.initialize(self)
	self.tupoy = true
	if #be.cyvasse.players > 2 then error('This AI support only two players mode!') end
end

function Tupoy:setStartingPosition()
	self.enemy = be.cyvasse.players[1]
	-- local king = Ai.findFirstUnit(self, 'king').cell
	Unit.static.randomPlacement(self.ai_player)
end

function Tupoy:nextTurn()
	if be.cyvasse.phase == 'reaver' then return end
		
	-- STEP 1 Добить короля
	local allies = Ai.playerUnits(self, self.ai_player)
	local enemy_king = Ai.findFirstUnit(self, 'king', self.enemy)
	
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone()
		for j, z in ipairs(zone) do
			if z.unit == enemy_king then
				v:fullMove(z)
				zz = 'STEP1'; return
			end
		end
	end
	
	-- STEP 2 Спасаем короля
	local enemies = Ai.playerUnits(self, self.enemy)
	local king = Ai.findFirstUnit(self, 'king', self.ai_player)
	local king_zone = king:getMovementZone()
	
	for i, v in ipairs(enemies) do
		local zone = v:getMovementZone()
		for j, z in ipairs(zone) do
			--  and v:checkCellAvaible(z, true)
			if z.unit == king then
				-- Тактическое отступление своего короля
				if #king_zone > 0 then
					king:fullMove(king_zone[love.math.random(1, #king_zone)])
					zz = 'STEP2'; return
				end
			end
		end
	end
	
	-- STEP 3 Атакуем случайную цель
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone() -- v.avaible_cells
		for j, z in ipairs(zone) do
			if v:checkCellAvaible(z, true) then
				-- Забираем случайную фигуру
				v:fullMove(z)
				zz = 'STEP3 ' .. v.cell.id .. ' ' .. z.id; return
			end
		end
	end
	
	-- STEP 4 Пока случайно перемещаем юнитов
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone() -- v.avaible_cells
		v:fullMove(zone[love.math.random(1, #zone)])
		zz = 'STEP4'; return
	end
end

return Tupoy