local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local Cell = require 'cyvasse.classes.cell'
local Ai = class('Ai')

function Ai:initialize()
end

function Ai:setStartingPosition()
end

function Ai:nextTurn()
	
end

function Ai:attachToPlayer(player)
	self.ai_player = player
end

function Ai:findFirstUnit(type, player)
	for i, v in ipairs(be.cyvasse.units) do
		if v.player == player and v.type == type then
			return v
		end
	end
end

function Ai:findFirstSafeCell(unit)
	local zone = unit:getMovementZone()
	for j, z in ipairs(zone) do
		if #Ai.findAttackersForCell(self, unit, z) == 0  then
			return z
		end
	end
	return nil
end

function Ai:getRandomFreeAvaibleCellInRow(row_symbol)
	local rows = { A = 5, B = 6, C = 7, D = 8, E = 9, F = 10, G = 9, H = 8, I = 7, K = 6, L = 5 }
	local cells = {}
	for i = 1, rows[row_symbol] do
		local cell = Cell.findById(row_symbol .. i)
		if not cell.unit then
			table.insert(cells, cell)
		end
	end
	if #cells > 0 then
		return cells[love.math.random(1, #cells)]
	else
		return nil
	end
end

function Ai:findAttackersForCell(unit, cell)
	local attackers = {}
	for i, v in ipairs(Ai.noPlayerUnits(self, unit.player)) do		
		local zone = v:getMovementZone()
		for j, z in ipairs(zone) do
			if z == cell then
				table.insert(attackers, v)
			end
		end
	end
	return attackers
end

function Ai:findAttackers(unit)
	local attackers = {}
	for i, v in ipairs(Ai.noPlayerUnits(self, unit.player)) do		
		local zone = v:getMovementZone()
		for j, z in ipairs(zone) do
			if z.unit == unit then
				table.insert(attackers, v)
			end
		end
	end
	return attackers
end

function Ai:findFirstAttacker(unit)
	local attackers = Ai.findAttackers(self, unit)
	if #attackers > 0 then
		return attackers[1]
	else
		return nil
	end
end

function Ai:playerUnits(player)
	local a = {}
	for i, v in ipairs(be.cyvasse.units) do
		if v.player == player then
			table.insert(a, v)
		end
	end
	return a
end

function Ai:noPlayerUnits(player)
	local a = {}
	for i, v in ipairs(be.cyvasse.units) do
		if v.player ~= player then
			table.insert(a, v)
		end
	end
	return a
end

return Ai