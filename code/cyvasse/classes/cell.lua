local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local fd = require 'fastdraw'
local utils = require 'utils'
local Cell = class('Cell')

local colorIndex = 0
local colors = { '#ffffff', '#7b917b', '#46394b', '#967117', '#9b8127', '#464451', '#646b63', '#472a3f', '#8b8c7a', '#806b2a', '#734222' }

function nextColor()
	colorIndex = colorIndex + 1
	if colorIndex > #colors then colorIndex = 1 end
	return colors[colorIndex]
end

Cell.static.width = 50 -- 70
Cell.static.height = 30 -- 40
Cell.static.hypotenuse = 51.478150704935004
Cell.static.angle = 0.507098504392337

Cell.static.findById = function(id)
	for i, v in ipairs(be.cyvasse.cells) do
		if v.id == id then
			return v, i
		end
	end
end

Cell.static.findIndex = function(cell)
	for i, v in ipairs(be.cyvasse.cells) do
		if v == cell then
			return i
		end
	end
end

Cell.static.updateAllIndexex = function()
	for i, v in ipairs(be.cyvasse.cells) do
		v:updateIndex()
	end
end

function Cell:initialize(x, y, id, zone, row)
	self.x = x and x or 150
	self.y = y and y or 150
	self.id = id and id or nil
	self.zone = zone
	if zone == 'mid' then
		self.row = 0
	elseif zone == 'top' then
		self.row = row
	elseif zone == 'bot' then
		self.row = -row
	else
		self.row = '/'
	end
	self.color = nextColor()
	self.rect = {
		self.x - (Cell.static.width / 4),
		self.y - (Cell.static.height),
		Cell.static.width * 1.5,
		Cell.static.height * 3
	}
	self.poly = self:getPoly(x, y, Cell.static.width, Cell.static.height)
	self.flatPoly = self:getFlatPoly(x, y, Cell.static.width, Cell.static.height)
	self.flatPolyInner = self:getFlatPoly(x + 2, y + 2, Cell.static.width - 4, Cell.static.height - 4)
	-- self.rpoly = self:getRPoly(x, y, Cell.static.width, Cell.static.height)
	self.hovered = false
	self.pathable = false
	self.pathes = {}
	self.avaibleZone = {}
	self.unit = nil
	self.index = nil
	table.insert(be.cyvasse.cells, self)
end

function Cell:ontop()
	local index = Cell.static.findIndex(self)
	table.remove(be.cyvasse.cells, index)
	table.insert(be.cyvasse.cells, self)
	
	Cell.static.updateAllIndexex(be.cyvasse.cells)
end

function Cell:onbot(cells)
	local index = Cell.static.findIndex(self)
	table.remove(be.cyvasse.cells, index)
	table.insert(be.cyvasse.cells, 1, self)
	
	Cell.static.updateAllIndexex(be.cyvasse.cells)
end

function Cell:updateIndex()
	self.index = Cell.static.findIndex(self)
end

function Cell:mousecheck(x, y)
	if utils.inside_rectangle_o(x, y, self.rect) then
		if utils.point_inside_poly(x, y, self.poly) then
			return self
		end
	end
end

function Cell:select()
	self.hovered = true
	for i, v in ipairs(self.pathes) do
		v.pathable = true
	end
end

function Cell:deselect()
	self.hovered = false
	for i, v in ipairs(self.pathes) do
		v.pathable = false
	end
end

function Cell:getPoly(x, y, width, height)
	local half_width = width / 2
	local half_height = height / 2
	return {
		{ x, y },
		{ x + half_width, y - half_height },
		{ x + width, y },
		{ x + width, y + height },
		{ x + half_width, y + height + half_height },
		{ x, y + height },
		{ x, y }
	}
end

function Cell:getFlatPoly(x, y, width, height)
	local half_width = width / 2
	local half_height = height / 2
	return {
		x, y,
		x + half_width, y - half_height,
		x + width, y,
		x + width, y + height,
		x + half_width, y + height + half_height,
		x, y + height
	}
end

function Cell:getRPoly(x, y, width, height)
	local half_width = width / 2
	local half_height = height / 2
	return utils.to_r_array({
		7 * 2 + 1,
		x, y,
		x + half_width, y - half_height,
		x + width, y,
		x + width, y + height,
		x + half_width, y + height + half_height,
		x, y + height,
		x, y
	})
end

function Cell:draw()
	fd.color(not self.unit and '#ffffff' or self.unit.player.color).pol6x('line', self.flatPoly)
	.pol6x('fill', self.flatPoly)
	
	local color
	if self.hovered then
		color = '#ff0000'
	elseif self.pathable then
		color = '#ffcbdb'
	else
		color = '#000000'
	end
	
	fd.color(color).pol6x('line', self.flatPolyInner)
	.pol6x('fill', self.flatPolyInner)
	
--	if self.unit then
--		self.unit:draw()
--	end
	
--	fd.color('#ffffff').print(self.index or 'X', self.x + 15, self.y + 15)
--	if self.hovered then
--		fd.color(self.color).rectangle('line', unpack(self.rect))
--	end
end

return Cell