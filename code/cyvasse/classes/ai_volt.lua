local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local Ai = require 'cyvasse.classes.ai'
local Unit = require 'cyvasse.classes.unit'
local Cell = require 'cyvasse.classes.cell'

local Volt = class('Volt', Ai)

function Volt:initialize()
	Ai.initialize(self)
	self.tupoy = true
	if #be.cyvasse.players > 2 then error('This AI support only two players mode!') end
end

function Volt:setStartingPosition()
	self.enemy = be.cyvasse.players[1]
	
--	local zone_cells = {}
--	for i, v in ipairs(be.cyvasse.cells) do
--		if v.zone == self.ai_player.zone then
--			table.insert(zone_cells, v)
--		end
--	end
	
	-- Размещаем короля в третьем ряду и сразу перед ним катапульту и требушет
	local king = Ai.findFirstUnit(self, 'king', self.ai_player)
	local kcl = love.math.random(1, 9)
	king:changeCell(Cell.findById('H' .. kcl))
	
	local catapult = Ai.findFirstUnit(self, 'catapult', self.ai_player)
	catapult:changeCell(Cell.findById('G' .. kcl))
	
	local trebuchet = Ai.findFirstUnit(self, 'trebuchet', self.ai_player)
	trebuchet:changeCell(Cell.findById('G' .. (kcl + 1)))
	
	-- Размещаем по одно лошадке каждого типа к ближайшей доступной линии к центру (т.е. в линии G)
	local lighthorse = Ai.findFirstUnit(self, 'lighthorse', self.ai_player)
	lighthorse:changeCell(Ai:getRandomFreeAvaibleCellInRow('G'))
	local heavyhorse = Ai.findFirstUnit(self, 'heavyhorse', self.ai_player)
	heavyhorse:changeCell(Ai:getRandomFreeAvaibleCellInRow('G'))
	
	local mountain = Ai.findFirstUnit(self, 'mountain', self.ai_player)
	mountain:changeCell(Ai:getRandomFreeAvaibleCellInRow('H'))
	
	Unit.static.randomPlacement(self.ai_player)
end

function Volt:nextTurn()
	if be.cyvasse.phase == 'reaver' then return end
		
	-- STEP 1 Добить короля
	local allies = Ai.playerUnits(self, self.ai_player)
	local enemy_king = Ai.findFirstUnit(self, 'king', self.enemy)
	
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone()
		for j, z in ipairs(zone) do
			if z.unit == enemy_king then
				v:fullMove(z)
				zz = 'STEP VATAKU'; return
			end
		end
	end
	
	-- STEP 2 Спасаем короля
	local enemies = self:playerUnits(self.enemy)
	local king = self:findFirstUnit('king', self.ai_player)
	local king_zone = king:getMovementZone()
	
	local attackers = self:findAttackers(king)
	if #attackers == 1 then
		-- Если атакующий один
		-- Находим потенциального защитника, который извергнет свой гнев на атакующего
		local potential_defender = self:findFirstAttacker(attackers[1])
		if potential_defender then
			zz = 'STEP POTDEF'
			return potential_defender:fullMove(attackers[1].cell)
		else
			-- Защитника нет, пора королю сваливать
			local safe_cell = self:findFirstSafeCell(king)
			if safe_cell then
				-- Отступаем на эту безопасную клетку
				zz = 'STEP SAFECELL'
				return king:fullMove(safe_cell)
			else
				-- Следовательно безопасной для отступления клетки нет, отступаем в случайную клетку в надежде надурить игрока
				zz = 'STEP BEDA ' .. king.cell.id
				if #king_zone > 0 then
					return king:fullMove(king_zone[love.math.random(1, #king_zone)])
				end
			end
		end
	elseif #attackers > 1 then
		-- Здесь так же можно рассмотреть вариант с блокировкой пути вражеским юнитам к королю (будет в АИ bordel)
		
		-- Если атакующих 2 и более, следовательно единственный вариант - убегать самим королём
		local safe_cell = self:findFirstSafeCell(king)
		if safe_cell then
			-- Отступаем на эту безопасную клетку
			zz = 'STEP SAFECELL MULTIATTACK ' .. king.cell.id .. '->' .. safe_cell.id
			return king:fullMove(safe_cell)
		else
			-- Следовательно безопасной для отступления клетки нет, пытаемся уничтожить одного из атакующих в надежде надурить игрока
			for i, a in ipairs(attackers) do
				local potential_defender = self:findFirstAttacker(a)
				if potential_defender then
					zz = 'STEP MULTIATTACK NAEBDEF'
					return potential_defender:fullMove(attackers[1].cell)
				end
			end
		
			-- Псевдозащитника нет, тогда просто отступаем в случайную клетку в надежде надурить игрока
			zz = 'STEP BEDA MULTIATTACK ' .. king.cell.id
			if #king_zone > 0 then
				return king:fullMove(king_zone[love.math.random(1, #king_zone)])
			end
		end
	end
	
	-- STEP 3 Атакуем случайную цель
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone() -- v.avaible_cells
		for j, z in ipairs(zone) do
			if v:checkCellAvaible(z, true) then
				-- Забираем случайную фигуру
				v:fullMove(z)
				zz = 'STEP SLUCHKA ' .. v.cell.id .. ' ' .. z.id; return
			end
		end
	end
	
	-- STEP 4 Пока случайно перемещаем юнитов
	for i, v in ipairs(allies) do
		local zone = v:getMovementZone() -- v.avaible_cells
		if zone ~= nil and #zone > 0 then
			v:fullMove(zone[love.math.random(1, #zone)])
			zz = 'STEP GO'; return
		end
	end	
end

return Volt