local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'

local Player = class('Player')

function Player:initialize(color, side)
	self.color = color
	self.type = 'player'
	self.zone = side
	if side =='bot' then
		local start_x = (love.graphics.getWidth() - #be.cyvasse.units_pool * 50) / 2
		for i, u in ipairs(be.cyvasse.units_pool) do
			require('cyvasse.classes.placed_cell'):new(start_x + (i - 1) * 50, 640, u, self, u .. i)
		end
	elseif side == 'top' then
		local start_x = (love.graphics.getWidth() - #be.cyvasse.units_pool * 50) / 2
		for i, u in ipairs(be.cyvasse.units_pool) do
			require('cyvasse.classes.placed_cell'):new(start_x + (i - 1) * 50, 50, u, self, u .. i)
		end
	end
	table.insert(be.cyvasse.players, self)
end

function Player:setAi(ai)
	self.type = 'ai'
	self.ai = ai
	self.ai:attachToPlayer(self)
end

function Player.static.nextTurn()
	be.cyvasse.turn = be.cyvasse.turn + 1
	if be.cyvasse.turn > #be.cyvasse.players then
		be.cyvasse.turn = 1
	end
	if be.cyvasse.players[be.cyvasse.turn].type == 'ai' then
		be.cyvasse.players[be.cyvasse.turn].ai:nextTurn()
	end
end

return Player