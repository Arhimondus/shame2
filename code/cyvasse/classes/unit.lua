local be = require 'bidons'
local assets = require('cargo').init('assets')

local class = require 'middleclass'
local fd = require 'fastdraw'
local utils = require 'utils'
local lume = require 'lume'
local flux = require 'flux'
local Cell = require 'cyvasse.classes.cell'
local Player = require 'cyvasse.classes.player'

local Unit = class('Unit')

Unit.static.findIndex = function(unit)
	for i, v in ipairs(be.cyvasse.units) do
		if v == unit then
			return i
		end
	end
end

function Unit:initialize(type, player, cell)
	self.type = type
	self.player = player
	if cell then
		self.cell = cell
		self.cell.unit = self
	end
	self.moving = false
	self.dragging = false
	self.shader = love.graphics.newShader [[
		extern vec4 from_color; 
		extern vec4 to_color;
		vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
		  vec4 pixel = Texel(texture, texture_coords );
          if(pixel == from_color) {
		    return to_color;  
		  }  
		  return pixel;
		}
	]]
	self.shader:send('from_color', lume.colort('#ff0000'))
	self.shader:send('to_color', lume.colort(player and player.color or '#ffffff'))
	self:resetPosition()
	self:setDefaultParams()
	table.insert(be.cyvasse.units, self)
end

function Unit:setDefaultParams()
	if self.type == 'rabble' then
		self.strength = 1
		self.movement = 3
		self.trump = nil
	elseif self.type == 'spearman' then
		self.strength = 2
		self.movement = 2
		self.trump = 'lighthorse'
	elseif self.type == 'elephant' then
		self.strength = 4
		self.movement = 3
		self.trump = nil
	elseif self.type == 'lighthorse' then
		self.strength = 2
		self.magnitude = 2
		self.movement = 3 * self.magnitude -- Временно удвоенная скорость
		self.trump = nil
	elseif self.type == 'heavyhorse' then
		self.strength = 3
		self.magnitude = 2
		self.movement = 2 * self.magnitude -- Временно удвоенная скорость
		self.trump = nil
	elseif self.type == 'crossbowman' then
		self.strength = 2
		self.movement = 1
		self.trump = 'elephant'
		self.range = 2
	elseif self.type == 'catapult' then
		self.strength = 3
		self.movement = 2
		self.trump = 'dragon'
		self.range = 3
	elseif self.type == 'trebuchet' then
		self.strength = 1
		self.movement = nil
		self.trump = 'dragon'
		self.range = 3
	elseif self.type == 'dragon' then
		self.strength = 5
		self.movement = nil
		self.trump = nil
		self.straight = true
	elseif self.type == 'king' then
		self.strength = 2
		self.movement = 2
		self.trump = nil
	elseif self.type == 'mountain' then
		self.strength = nil
		self.movement = nil
		self.trump = nil
		self.nopath = true
	end
end

function Unit:getMovementZone()
	local player_type = be.cyvasse.players[be.cyvasse.turn].type
	
	local unit = self
	local from = self.cell
	unit.avaible_cells = {}
	if self.straight then
		for i, to in ipairs(from.pathes) do
			for i = 1, 10 do
				local breakout = false
				
				local a = math.abs(from.x - to.x)
				local b = math.abs(from.y - to.y)
				local c = math.sqrt(a * a + b * b)
				
				local l_angle = math.atan(b / a)
				local s_angle = math.atan(a / b)
				local hc = Cell.static.hypotenuse * i
				
				local signx = (from.x - to.x) < 0 and 1 or -1
				local signy = (from.y - to.y) < 0 and 1 or -1
				
				local nx = from.x + signx * math.sin(s_angle) * hc / math.sin(math.pi / 2) + Cell.static.width / 2
				local ny = from.y + signy * math.sin(l_angle) * hc / math.sin(math.pi / 2)
				
				for i, v in ipairs(be.cyvasse.cells) do
					if v.class.name == 'Cell' then
						local mm = v:mousecheck(nx, ny)
						if mm then
							if not mm.unit then
								if player_type == 'player' then
									mm.pathable = true
								elseif player_type == 'ai' then
									table.insert(unit.avaible_cells, mm)
								end
							else
								if mm.unit.range and mm.unit.player ~= self.player then
									if self:checkCellAvaible(mm) then
										if player_type == 'player' then
											mm.pathable = true
										elseif player_type == 'ai' then
											table.insert(unit.avaible_cells, mm)
										end
									end
									breakout = true
									break
								elseif not mm.unit.nopath and mm.unit.player ~= self.player then
									if self:checkCellAvaible(mm) then
										if player_type == 'player' then
											mm.pathable = true
										elseif player_type == 'ai' then
											table.insert(unit.avaible_cells, mm)
										end
									end
								end
							end
						end
					end
				end
				if breakout then break end
			end
		end
	elseif self.movement then
		local all_avaible_cells = {}
		self:deepPathable(from, self.movement, all_avaible_cells)
		
		table.insert(all_avaible_cells, from)
		
		-- DEJKSTRA
		local remainings = lume.clone(all_avaible_cells)
		local infinite = 1000
		
		utils.each(remainings, function(c, i) c.weight = infinite; c.visited = nil; c.back = nil; end)
		
		from.weight = 0
		from.back = nil
		
		repeat
--			local remainingsX = utils.select(remainings, function(r) return r.id end)
--			local avaiblesX = utils.select(unit.avaible_cells, function(r) return r.id end)
			local vertex = utils.findMinimal(remainings, 'weight')
			
			if vertex.path_type ~= 'end' or vertex == from then
				for i, v in ipairs(vertex.pathes) do
					if lume.find(remainings, v) then
						local new_weight = vertex.weight + 1
						if new_weight < v.weight then
							v.weight = new_weight
							v.back = vertex
						end
					end
				end
			end
			table.remove(remainings, lume.find(remainings, vertex))
		until(#remainings == 0)
		
		if self.range then
			self:deepPathableRange(from, self.range, unit.avaible_cells, player_type)
		end
		
		for i, v in ipairs(all_avaible_cells) do
			if v.weight <= self.movement and self ~= v.unit then
				if player_type == 'player' then
					v.pathable = true
--					table.insert(from.avaible_cells, v)
				elseif player_type == 'ai' then
					table.insert(unit.avaible_cells, v)
				end
			end
		end
	elseif self.range then
		self:deepPathableRange(from, self.range, unit.avaible_cells, player_type)
	end
--	zz = #unit.avaible_cells
	return unit.avaible_cells
end

function Unit:deepPathableRange(cell, count, avaibleCells, player_type)
	for i, v in ipairs(cell.pathes) do
		if v ~= self.cell then
			if self:checkCellAvaible(v, true) then
				if player_type == 'player' then
					v.pathable = true
				elseif player_type == 'ai' then
					table.insert(avaibleCells, v)
				end
			end
		end
			
		if count > 1 then
			self:deepPathableRange(v, count - 1, avaibleCells, player_type)
		end	
	end
end

function Unit:deepPathable(cell, count, avaibleCells)
	for i, v in ipairs(cell.pathes) do
		local ca = self:checkCellAvaible(v)
		if ca then
			if not lume.find(avaibleCells, v) then
--				v.pathable = true
				v.path_type = ca
				table.insert(avaibleCells, v)
			end
			if count > 1 then
				self:deepPathable(v, count - 1, avaibleCells)
			end	
		else
			v.path_type = nil
		end
	end
end

function Unit:checkCellAvaible(cell, dont_check_empty)
	if not cell.unit then
		if not dont_check_empty then
			return 'free'
		else
			return false
		end
	end
	
--	if cell.unit.type == 'trebuchet' then
--		print ''
--	end

	if not cell.unit.strength then return false end
	if cell.unit == self then return 'self' end
	if cell.unit.player == self.player then return false end
	
	local attacker = self
	local defender = cell.unit
	
	-- CHECK TRUMP
	if attacker.trump and attacker.trump == defender.type then
		return 'end'
	end
	
	if defender.trump and defender.trump == attacker.type then
		return false
	end
	
	if defender.range then
		return 'end'
	end

	-- CHECK STRENGTH
	if attacker.strength >= defender.strength then
		return 'end'
	end
	
	return false
end

function Unit:resetPosition()
	self.x = self.cell.x
	self.y = self.cell.y - 18
end

function Unit:ontop()
	local index = Unit.static.findIndex(self)
	table.remove(be.cyvasse.units, index)
	table.insert(be.cyvasse.units, self)
end

function Unit:startDragging()
	for i, v in ipairs(be.cyvasse.cells) do
		if v.class.name == 'Cell' then
			v.back = nil
		end
	end
	
	local player = be.cyvasse.players[be.cyvasse.turn]
	if self.player ~= player then return end
	
	-- self.cell:ontop(be.cyvasse.cells)
	self:resetPosition()
	if be.cyvasse.phase == 'preflop' then
		for i, v in lume.ripairs(be.cyvasse.cells) do
			if v.class.name == 'Cell' and v.zone == 'bot' then
				v.pathable = true
			end
		end
		self.dragging = true
	elseif be.cyvasse.phase == 'flop' then
		self:ontop()
		self:getMovementZone()
		self.dragging = true
	end
end

function Unit:stopDragging(x, y)
	for i, v in lume.ripairs(be.cyvasse.cells) do		
		if v ~= self.cell then
			local mm = v:mousecheck(x, y)
			if mm then
				if mm.pathable then
					if be.cyvasse.phase == 'preflop' then
						self:changeCell(mm)
					elseif be.cyvasse.phase == 'flop' then
						be.cyvasse.phase = 'turn'
						if not self.straight then
							-- Begin unit moving
							-- Restore Dejkstra path
							local path = {}
							if mm.back ~= nil then
								local cell = mm
								repeat
									table.insert(path, 1, cell.id)
									cell = cell.back
								until(cell.back == nil)
							else
								table.insert(path, 1, mm.id)
							end
							
							local unit = self
							local unit_to = Cell.static.findById(path[#path]).unit
							local old_cell = unit.cell
							
							be.cyvasse.moving_cell = Unit.static.startMovingPathById(self.cell.id, path, function()
								be.cyvasse.moving_cell = nil
								if be.cyvasse.phase ~= 'reaver' then
									be.cyvasse.phase = 'flop'
								end
								if unit_to and unit.range then
									unit:changeCell(old_cell)
									unit:resetPosition()
								end
								Player.static.nextTurn()
							end)
						else
							local flux
							be.cyvasse.moving_cell, flux = Unit.static.startMoving(self, mm)
							flux:oncomplete(function()
								be.cyvasse.moving_cell = nil
								if be.cyvasse.phase ~= 'reaver' then
									be.cyvasse.phase = 'flop'
								end
								Player.static.nextTurn()
							end)
						end
					end
					-- mm:ontop(cells)
				end
				-- self.dragging = false
			end
		end
	end
	self:resetPosition()
	for i, v in ipairs(be.cyvasse.cells) do
		v.pathable = nil
	end
	self.dragging = false
end

function Unit:fullMove(mm)
	for i, v in ipairs(be.cyvasse.cells) do
		if v.class.name == 'Cell' then
			v.back = nil
		end
	end
	self:getMovementZone()
	
	be.cyvasse.phase = 'turn'
	if not self.straight then
		-- Begin unit moving
		-- Restore Dejkstra path
		local path = {}
		if mm.back ~= nil then
			local cell = mm
			repeat
				table.insert(path, 1, cell.id)
				cell = cell.back
			until(cell.back == nil)
		else
			table.insert(path, 1, mm.id)
		end
		
		if self.movement then
			if #path > self.movement then
				error('EE' .. self.cell.id .. path[1])
			end
		end
	
		local unit = self
		local unit_to = Cell.static.findById(path[#path]).unit
		local old_cell = unit.cell
		
		be.cyvasse.moving_cell = Unit.static.startMovingPathById(self.cell.id, path, function()
			be.cyvasse.moving_cell = nil
			if be.cyvasse.phase ~= 'reaver' then
				be.cyvasse.phase = 'flop'
			end
			if unit_to and unit.range then
				unit:changeCell(old_cell)
				unit:resetPosition()
			end
			Player.static.nextTurn()
		end)
	else
		local flux
		be.cyvasse.moving_cell, flux = Unit.static.startMoving(self, mm)
		flux:oncomplete(function()
			be.cyvasse.moving_cell = nil
			if be.cyvasse.phase ~= 'reaver' then
				be.cyvasse.phase = 'flop'
			end
			Player.static.nextTurn()
		end)
	end
end

function Unit:startMovingById(to_id)
	return Unit.static.startMovingById(self.cell.id, to_id)
end

function Unit:startMoving(to)
	return Unit.static.startMoving(self.cell, to)
end

Unit.static.startMovingPathById = function(from_id, path_table, oncomplete)
	local from = Cell.static.findById(from_id)
	local to = utils.select(path_table, function(id) return Cell.static.findById(id) end)
	return Unit.static.startMovingPath(from, to, oncomplete)
end

Unit.static.startMovingPath = function(from, path_table, oncomplete)
	local unit = from.unit
	if not unit then error('No unit in that cell!') end
	unit:ontop()
	unit.moves = {}
	unit.moving = true
	for i, to in ipairs(path_table) do
		table.insert(unit.moves, function()
			local _, fluxable = Unit.static.startMoving(unit, to)
			fluxable:oncomplete(function() 
				unit.moves[i + 1]()
			end)
		end)
	end
--	for i, to in lume.ripairs(path_table) do
--		to:ontop(cells)
--	end
--	from:ontop(cells)
	table.insert(unit.moves, function()
		unit.moving = false
		oncomplete()
	end)
	unit.moves[1]()
	return unit
end

Unit.static.startMovingById = function(from_id, to_id)
	local from = Cell.static.findById(from_id)
	local to = Cell.static.findById(to_id)
	return Unit.static.startMoving(from, to)
end

Unit.static.startMoving = function(from, to, duration)
	local unit
	if from.class.name == 'Unit' then
		unit = from
		from = unit.cell
	elseif from.class.name == 'Cell' then
		unit = from.unit
	end
	
--	local from_index = Cell.static.findIndex(from)
--	table.remove(be.cyvasse.cells, from_index)
	
--	local to_index = Cell.static.findIndex(to)
--	table.remove(be.cyvasse.cells, to_index)
	
--	table.insert(be.cyvasse.cells, 1, from)
--	table.insert(be.cyvasse.cells, 1, to)
	
	local a = math.abs(from.x - to.x)
	local b = math.abs(from.y - to.y)
	local c = math.sqrt(a * a + b * b)
	
	unit.l_angle = math.atan(b / a)
	unit.s_angle = math.atan(a / b)
	unit.hc_width = 0
	unit.signx = (from.x - to.x) < 0 and 1 or -1
	unit.signy = (from.y - to.y) < 0 and 1 or -1
	unit.moving = true
	
	if not duration then
		if unit.type == 'dragon' then
			duration = 1
		else
			duration = 0.5
		end
	end
	
	return unit, flux.to(unit, duration, { hc_width = math.floor(c) }):oncomplete(function()
		unit.moving = false
		unit:changeCell(to)
	end)
end

function Unit.static.randomPlacement(player)
	local zone_cells = {}
	for i, v in ipairs(be.cyvasse.cells) do
		if v.zone == player.zone then
			table.insert(zone_cells, v)
		end
	end
	
	for i, v in ipairs(be.cyvasse.cells) do
		if v.class.name == 'PlacedCell' and v.unit and v.unit.player == player then
			while true do
				local random = love.math.random(1, #zone_cells)
				if zone_cells[random].unit == nil then
					v.unit:changeCell(zone_cells[random])
					break
				end
			end
		end
	end
	
	for i, v in ipairs(be.cyvasse.units) do
		v:resetPosition()
	end
end

function Unit:changeCell(cell)
	if cell.unit ~= nil then
		if cell.unit.type == 'king' then
			be.cyvasse.phase = 'reaver'
			be.cyvasse.result = cell.unit.player.type == 'player' and 'defeat' or 'victory'
		end
		local index = Unit.static.findIndex(cell.unit)
		table.remove(be.cyvasse.units, index)
	end
	cell.unit = self
	self.cell.unit = nil
	self.cell = cell
end

function Unit:destroyTarget(cell)
	if cell.unit ~= nil then
		local index = Unit.static.findIndex(cell.unit)
		table.remove(be.cyvasse.units, index)
	end
	cell.unit = nil
end

function Unit:update(dt)
	if self.moving then
		local nx = self.cell.x + self.signx * math.sin(self.s_angle) * self.hc_width / math.sin(math.pi / 2)
		local ny = self.cell.y + self.signy * math.sin(self.l_angle) * self.hc_width / math.sin(math.pi / 2)
		self.x = math.floor(nx)
		self.y = math.floor(ny) - 18
	end
end

function Unit:draw()
	love.graphics.setShader(self.shader)
	fd.color().draw(assets.cyvasse[self.type], self.x, self.y)
	love.graphics.setShader()
end

return Unit