be = require 'bidons'
wae = require 'wae'
inspect = require 'inspect'

GeChapter = require 'classes.ge_chapter'
GePlayer = require 'classes.ge_player'
GeChar = require 'classes.ge_char'
GeItem = require 'classes.ge_item'

Chapter1 = require('middleclass')('Chapter1', GeChapter)

Chapter1.initialize = () =>
	GeChapter.initialize(@)
	
	@context.bgl = (msg) =>
		(require 'bidons').utilitaryChannel\push(msg)

	@context\bgl('gamestarted')

	with @context
		\addChar GePlayer("Игрок", "#aaaaaa")
		\addChar GeChar("Narrator", "Нарратор", "#aabbcc00")
		\addChar GeChar("Dorn", "Дорн", "#ba9b28")
		\addChar GeChar("Cersei", "Серсея", "#d35243")
		\addChar GeChar("Sansa", "Санса", "#ffca44")
		
		\addChar GeChar("Tiena", "Тиена", "#ba9b28")
		\addChar GeChar("Elvira", "Эльвира", "#bc45b4")
		\addChar GeChar("Leyna", "Лейна", "#af15a2")

		\addChar GeChar("Iren0", "Владелица ателье", "#8486bc")
		\addChar GeChar("Iren", "Ирэн", "#8486bc")

		\addChar GeChar("Madam0", "Управляющая", "#b5807c")
		\addChar GeChar("Madam", "Мадам", "#b5807c")

		\addChar GeChar("Sungar0", "Мастер над кораблями", "#528761")
		\addChar GeChar("Sungar", "Госпожа Сангар", "#528761")

		\addItem GeItem("list_port", "Монага", "trap", "#527f3f90", true)
		\addItem GeItem("list_jewerly", "Вряга", "branch", "#ff800090", true)
		\addItem GeItem("list_blacksmith", "Шняга", "boots", "#ea000099")

	with @context
		.girls = { .Cersei, .Tiena, .Sansa, .Elvira, .Leyna, .Iren, .Madam, .Sungar }

	@context.getCharColor = (ctx, bdl_char) ->
		char = fr(ctx.chars)\whereKeys((a) ->
			a.value.name == bdl_char)\first()
		if char
			return char.value.color
		else
			return '#aaaaaa'

	with @context.Player
		\giveBdlItem('Монага', 1)
		\giveBdlItem('Вряга', 2)
		\giveBdlItem('Шняга', 3)
		\giveBdlItem('Монага', 1)
		\giveBdlItem('Вряга', 2)
		\giveBdlItem('Шняга', 3)

	@context.quests = require 'chapters.chapter1_quests'
	@context.achievements = require 'chapters.chapter1_achievements'
	
	require('chapters.chapter1_scenes')(@context.chars)
	require('chapters.chapter1_services')(@context.chars)
	require('chapters.chapter1_costumes')(@context.chars)
	
	@context.get_quests = () =>
		quests = fr(@quests)\where((e) -> e.value.started ~= nil and e.value.started == true)\reverse!\toArray!
		
		if #quests == 0
			return {}
		
		quests
		
	@context.get_quest = () =>
		if @position == nil
			@position = 1
		qs = @get_quests!
		@quest = qs[@position]
		
	@context.next_quest = () =>
		qs = @get_quests!
		@position += 1
		if @position > #qs
			@position = 1
		@quest = qs[@position]
	
	@context.prev_quest = () =>
		qs = @get_quests!
		@position -= 1
		if @position < 1
			@position = #qs
		@quest = qs[@position]
		
	@context.get_quests_stat = () =>
		all_quests = fr(@quests)\where((e) -> e.value.started ~= nil)\toArray!
		done_quests = fr(@quests)\where((e) -> e.value.started ~= nil and e.value.completed == true)\toArray!
		return {
			all: #all_quests,
			done: #done_quests,
			percentages: math.floor(#done_quests * 100 / #all_quests)
		}
		
	@context.get_scenes_stat = () =>
		all_scenes = fr(@girls)\selectManyKeyValues((e) -> e.scenes)\toArray!
		done_scenes = fr(all_scenes)\where((e) -> e.value == true)\toArray!
		return {
			all: #all_scenes,
			done: #done_scenes,
			percentages: math.floor(#done_scenes * 100 / #all_scenes)
		}
		
	@context.get_achievements_stat = () =>
		all_achievements = fr(@achievements)\toArray!
		done_achievements = fr(@achievements)\where((e) -> e.value == true)\toArray!
		return {
			all: #all_achievements,
			done: #done_achievements,
			percentages: math.floor(#done_achievements * 100 / #all_achievements)
		}

	@context.get_overall_progress = () =>
		qs = @get_quests_stat!.percentages
		sc = @get_scenes_stat!.percentages
		as = @get_achievements_stat!.percentages
		math.floor((qs + sc + as) * 100 / 300)
		
	@context.cyvasse = {
		result: nil,
		backstate: { 'kings_landing' },
		bet: 0
	}
	
	@context.guadeloupe = {
		top: {
			name: "Верхний герой",
			health: 40,
			rage: 0,
			units: {}
		},
		bottom: {
			name: "Нижний герой",
			health: 40,
			rage: 0,
			units: {}
		}
	}
	
	@context.endDay = () =>
		if @Player.money >= 100000 and not @achievements.Master_over_the_coin
			@achievements.Master_over_the_coin = true
			@bgl('ach-Master_over_the_coin')

		if @Player.gossip >= 1000 and not @achievements.Two_spiders_in_a_can
			@achievements.Two_spiders_in_a_can = true
			@bgl('ach-Two_spiders_in_a_can')

		if @Player.authority >= 1000 and not @achievements.This_is_my_city
			@achievements.This_is_my_city = true
			@bgl('ach-This_is_my_city')

		if @quests.Grand_opening.completed
			@Player.money -= math.floor(@bordel_power * 0.10)

		if @day % 3 == 0
			if be and be.saveloader
				be.saveloader\quickSave(false, true)

		if @day == 10
			@bgl('day-10')

		if @day == 25
			@bgl('day-25')
			
		if @day == 50
			@bgl('day-50')
			
		if @day == 100
			@bgl('day-100')
			
	@context.bordel_power = 0

	ge = require 'controllers.guadeloupe_extend'
	
	@context.Player.cards = {
		ge.cards['ariya_stark'],
		ge.cards['blackfish'],
		ge.cards['boltons'],
		ge.cards['bran_stark'],
		ge.cards['hodor'],
		ge.cards['loup_garou'],
		ge.cards['meera_reed'],
		ge.cards['robb_stark'],
		ge.cards['theon_greyjoy']
	}
	
	@context.Narrator.cards = {
		ge.cards['ariya_stark'],
		ge.cards['blackfish'],
		ge.cards['boltons'],
		ge.cards['bran_stark'],
		ge.cards['hodor'],
		ge.cards['loup_garou'],
		ge.cards['meera_reed'],
		ge.cards['robb_stark'],
		ge.cards['theon_greyjoy']
	}

--	be.konstabel\entrust('achievements')
--	be.konstabel\entrust('dialog', 'oberin')
--	be.konstabel\entrust('gallery')
--	be.konstabel\entrust('kings_landing')
--	be.konstabel\entrust('guadeloupe')
--	be.konstabel\entrust('mon_bordel')
--	be.konstabel\entrust('bordel')
	be.konstabel\entrust('main_menu')
--	be.konstabel\entrust('settings')
--	be.konstabel\entrust('cyvasse')
--	be.konstabel\entrust('birds')

Chapter1