local class = require 'middleclass'
local be = require 'bidons'

local GeChapter = require 'classes.ge_chapter'
local GePlayer = require 'classes.ge_player'
local GeChar = require 'classes.ge_char'
local GeItem = require 'classes.ge_item'

local Chapter = class('Chapter1', GeChapter)

function Chapter:initialize()
	GeChapter.initialize(self)
	
	self.context:addChar(GePlayer:new("Игрок", "#f9e8b8"))
	
	self.context:addChar(GeChar:new("Narrator", "Нарратор", "#aabbcc00"))
	self.context:addChar(GeChar:new("Katherin", "Кейтрин", "#c8ffc8", { relationship = 0, husband_memory = 10 }, true))
	self.context:addChar(GeChar:new("Manaoth", "Манаот", "#e6e600", { satisfaction = 0 }, true))
	
	self.context:addChar(GeChar:new("Guard", "Охранник", "#ffffff"))
	self.context:addChar(GeChar:new("Host", "Трактирщик", "#ffffff"))
	self.context:addChar(GeChar:new("OldMan", "Старик", "#ffffff"))
	
	self.context:addItem(GeItem:new("Trap", "Ловушка", "trap", "#527f3f90", true))
	self.context:addItem(GeItem:new("MoulderingBranch", "Трухлявая ветка", "branch", "#ff800090", true))
	self.context:addItem(GeItem:new("ShoeOfDeath", "Башмак смерти", "boots", "#ea000099"))
	self.context:addItem(GeItem:new("SockOfTrueShame", "Носок истинного позора", "socks", "#ea77e4ee"))
	self.context:addItem(GeItem:new("WuClanArmor", "Доспех клана Ву", "mail", "#b9f79e99"))
	self.context:addItem(GeItem:new("SwordGrease", "Жировая смазка для меча", "grease", "#705f03ee", true))
	self.context:addItem(GeItem:new("ValyrianDagger", "Валирийский кинжал", "dagger", "#84d0ffee"))
	self.context:addItem(GeItem:new("WildGirl", "Одичалая", "wild", "#c689b6ee", true))
	self.context:addItem(GeItem:new("Lamp", "Лампа", "lamp", "#ffee5bee"))
	self.context:addItem(GeItem:new("Torch", "Факел", "torch", "#ff691edd"))
	
	self.context:addItem(GeItem:new("SwordGreaseSpecial", "Особая жировая смазка для меча", "grease", "#705f03ff"))
	self.context:addItem(GeItem:new("PerfumeSpecial", "Редкий парфюм из трав", "perfume", "#e5fdffee"))
	
	be.konstabel:entrust('dialog', 'Первая встреча')
end

return Chapter