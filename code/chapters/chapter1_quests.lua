return {
	Upon_this_rock1 = {
		started = false,
		talk_with_hostess = false,
		silver_delivered = false,
		repair_started = false,
		Started_day = 0,
		talk_with_blacksmith = false,
		talk_with_workers = false,
		talk_with_shipmaster = false,
		talk_with_jewerly = false,
		completed = false,
	},
	
	Career_guidance = {
		started = false,
		Day_before_sex = 0,
		Day_before_decision = 0,
		Day_since_bought = 0,
		talk_with_Elvira = false,
		sex_with_Elvira = false,
		Elvira_agreed = false,
		dress_bought = false,
		completed = false
	},
	
	Still_waters = {
		started = false,
		talk_with_Tiena = false,
		day_after_meet = 0,
		sex = false,
		day_after_sex = 0,
		completed = false
	},
	
	Never_late = {
		started = false,
		got_Tiena_from_convent = false,
		get_to_measure_Leyna = false,
		dress_bought = false,
		day_since_bought = 0,
		completed = false
	},
	
	Grand_opening = {
		started = false,
		talk_with_madam = false,
		talk_with_Tiena = false,
		bought_wine = false,
		got_instructions = false,
		set_girls = false,
		completed = false
	},
	
	Agressive_expansion = {
		started = false,
		hire_workers = false,
		bought_materials = false,
		repair_started = 0,
		opened = false,
		day_opened = 0,
		completed = false
	},
	
	All_secret = {
		opened = false
	},
	
	DominatingCersei = {
		first_talk = false,
		Iren_talk = false,
		day = 0,
		get_dress = false
	},
	
	--Persons
	Jendry = {
		meeting = false
	},
	
	Lilfinger = {
		first_meet = false,
		talk_after_sansa = false,
		deal = false
	},
	
	Spider = {
		first_meet = false,
		talk_after_first_look = false,
		deal = false
	},
	
	Sansa = {
		first_meet = false,
		Talked_with_spider = false,
		agreed_to_be_licked = false,
		visited = false
	},
	
	Cersei = {
		first_meet = false,
		first_talk = false,
		fuck_with_Lancel = false,
		visited = false
	},
	
	Iren = {
		meeting = false,
		looking = false
	},
	
	Sungar = {
		meeting = false,
		looking = false
	},
	
	Jude = {
		meeting = false,
		looking = false
	},
	
	--Locations
	Tavern = {

	},
	
	Cyvasse = {
		gaming_day = 0
	}
}