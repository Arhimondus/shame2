local be = require 'bidons'

local flux = require 'flux'
local timer = require 'timer'

local class = require 'middleclass'
local utils = require 'utils'
local fd = require 'fastdraw'
local lume = require 'lume'
local NeBox = require 'elements.ne-box'

local ElShametainer = NeBox:subclass('ElShametainer')

function ElShametainer:initialize(scene, source, base, binding_object, binding_index, owner)
	NeBox.initialize(self, scene, source, base, binding_object, binding_index, owner)
	
	if not self.toast_box then
		local toast_node = scene:single_bscn("toast")
		toast_node.location = { 1000, 550 }
	
		self.toast_box = self.scene:loadElement(toast_node, self.location, self.binding_object, nil, self)
		self:add(self.toast_box)
	end
end

function ElShametainer:toast(message)
	if self.tween then
		self.tween:stop()
	end
	
	self.toast_box.text = message
	self.toast_box.visible = true
	self.toast_box.opacity = 0
	
	self.toast_box.xcord = 1260
	self.toast_box.ycord = 720
	
	self.tween = flux.to(self.toast_box, 0.5, { opacity = 1, ycord = 700 })
	:onupdate(function ()
		-- self.toast_box:refreshSubobjects()
	end)
	:ease("linear")
	
	:after(self.toast_box, 0.8, { opacity = 0 })
	:onupdate(function ()
		-- self.toast_box:refreshSubobjects()
	end)
	:ease("quintout")
	:delay(0.8)
	:oncomplete(function()
		self.toast_box.visible = false
	end)
	
end

function ElShametainer:load()
	if not self.toast_box then
		self.toast_box = self.scene:get('Toast')
		if self.toast_box then
			self.toast_box.visible = false
		end
	end
end

function ElShametainer:keyreleased(key)
	if key == 'escape' then
		local dialog = self.scene:get('default_dialog')
		if dialog and dialog.skipping then
			dialog:stop()
			do return end
		end
		be.konstabel:issue('pausemenu')		
		return
	elseif key == 'f8' then
		self.scene.bidons.saveloader:quickSave(false)
		self:toast(self.scene.bidons.transmanager:get('birds_done'))
		print 'ElShametainer:keyreleased f8 quickSaved'
	elseif key == 'f11' then
		self.scene.bidons.saveloader:quickLoad(nil)
		-- self:toast("Загружено успешно.")
		print 'ElShametainer:keyreleased f11 quickLoaded'
	else
		NeBox.keyreleased(self, key)
	end
end

return ElShametainer