be = require 'bidons'
fd = require 'fastdraw'
Dialog = require 'elements.el-dialog'
flux = require 'flux'

ExtendedDialog = require('middleclass')('ExDialog', Dialog)

ExtendedDialog.initialize = (...) =>
	Dialog.initialize(@, ...)

	@sprites = {}
	@background = nil
	
	with @functions
		.clothes = (clothes) =>
			char = @charobj
			sprite_object = @scene\get('sprite')
			draw_objs = {}
			for key, value in pairs(clothes)
				sprites = char.all_clothes[key .. value].sprites
				for i, v in ipairs(sprites)
					sprite = char.prefix .. v\gsub('#', key .. value) 
					image = love.graphics.newImage('assets/clothes/' .. sprite .. '.png')
					table.insert(draw_objs, image)
			@scene\get('clothes')\setArray(draw_objs)
	
		.emotion = (emotions) =>
			char = @charobj
			sprite_object = @scene\get('sprite')
			draw_objs = {}
			for key, value in pairs(emotions)
				sprites = char.all_clothes[key .. value].sprites
				for i, v in ipairs(sprites)
					sprite = char.prefix .. v\gsub('#', key .. value) 
					image = love.graphics.newImage('assets/emotions/' .. sprite .. '.png')
					table.insert(draw_objs, image)

			@scene\get('emotions')\setArray(draw_objs)

		.scene = (scene, params) =>
			@end_dialog!
			if scene == 'bordel' or scene == 'kings_landing'
				@scene.bidons.konstabel\forceEntrust(scene, params)
			else
				@scene.bidons.konstabel\entrust(scene, params)

		.sprite = (char, options) =>
			sprite = 'assets/sprites/' .. char .. '.png'
			table.insert(@sprites, sprite)
			@scene\get('sprites')\updateBinding()
			
			index = #@sprites
			
			if options and not @scene.skipping
				options.time = options.time and options.time or 4
				options.ease = options.ease and options.ease or 'linear'
				options.xoffset = options.xoffset and options.xoffset or -250
				sprite_box = @scene\get('sprite_num#' .. index)
				sprite_box.opacity = 0
				initial_location_x = sprite_box.xcord
				sprite_box.xcord = sprite_box.xcord + options.xoffset
				flux.to(sprite_box, options.time, { opacity: 1, xcord: initial_location_x })\ease(options.ease)

		.off_sprite = (position) =>
			position = position == nil and 1 or position
			table.remove(@sprites, position)
			@scene\get('sprites')\updateBinding()
			
		.clear_sprites = () =>
			@sprites = {}
			@scene\get('sprites')\updateBinding()

		.clothes_reset = () =>
			if @charobj
				@functions.clothes(@, @charobj.default_clothes)

		.emotion_reset = () =>
			if @charobj
				@functions.emotion(@, @charobj.default_emotion)

		.give_item = (bdl_name, count) =>
			be.context.Player\giveBdlItem(bdl_name, count)
		
		.has_item = (bdl_name) =>
			be.context.Player\hasBdlItem(bdl_name)
		
		.remove_item = (bdl_name) =>
			be.context.Player\removeBdlItem(bdl_name)
			
		-- .give_object_item = (self, ...) ->
			-- be.context.Player\giveItem(...)

		.background = (background, options) =>
			backimage_box = @scene\get('backimage')
			if options and not @scene.skipping then
				options.time = options.time and options.time or 1
				options.ease = options.ease and options.ease or 'linear'
				backimage_box.opacity = 0
			else
				backimage_box.opacity = 1
			
			@background = 'assets/backgrounds/' .. background .. '.jpg'
			backimage_box\updateImage()
			
			if options and not @scene.skipping then
				flux.to(backimage_box, options.time, { opacity: 1 })\ease(options.ease)

		.close = () =>
			@functions.scene(@, 'kings_landing')

		.g_setup = () =>
			be.context.girls = be.context.allgirls
			@functions.scene(@, "mon_bordel")

		.random = (number) =>
			love.math.random(number)
			
		.log = (msg) =>
			be.context\bgl(msg)
			
		.autosave = () =>
			st = @scene\get('dialog')
			if st
				@scene.bidons.saveloader\quickSave(false, true)
				st\toast(@scene.bidons.transmanager\get('birds_done'))

ExtendedDialog.load_script = (script) =>
	Dialog.load_script(@, 'scripts/' .. script)

ExtendedDialog.end_dialog = () =>
	Dialog.end_dialog(@)
	
	@background = nil
	@sprites = {}
	
	@scene\get('sprites')\updateBinding()

ExtendedDialog.print = (msg) =>
	-- print(msg)

ExtendedDialog.pack = () =>
	packed = Dialog.pack(@)
	packed.packed = true
	packed.sprites = @sprites
	packed.background = @background
	packed

ExtendedDialog.unpack = () =>
	Dialog.unpack(@)
	@sprites = @source.sprites
	@background = @source.background
	@scene\get('sprites')\updateBinding!
	@scene\get('backimage')\updateImage!

return ExtendedDialog