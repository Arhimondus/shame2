be = require 'bidons'
fd = require 'fastdraw'
flux = require 'flux'
Box = require 'elements.ne-box'

BoxTainer = require('middleclass')('BoxTainer', Box)

BoxTainer.initialize = (scene, ...) =>
	Box.initialize(@, scene, ...)

	if not @toast_box
		toast_node = scene\single_bscn("toast")
		toast_node.location = { 1000, 550 }
	
		@toast_box = @scene\loadElement(toast_node, @location, @binding_object, nil, @)
		@add(@toast_box)
	
	@scene.toast = (message) =>
		@objects[1]\toast(message)
		
BoxTainer.toast = (message) =>
	if @tween
		@tween\stop()
	
	@toast_box.text = message
	@toast_box.visible = true
	@toast_box.opacity = 0
	
	@toast_box.xcord = 1260
	@toast_box.ycord = 720
	
	@tween = flux.to(@toast_box, 0.5, { opacity: 1, ycord: 700 })\ease("linear")\after(@toast_box, 0.8, { opacity: 0 })\ease("quintout")\delay(0.8)\oncomplete(() -> @toast_box.visible = false)

BoxTainer.load = () =>
	if not @toast_box
		@toast_box = @scene\get('Toast')
		if @toast_box
			@toast_box.visible = false

return BoxTainer