local class = require 'middleclass'
local be = require 'bidons'
local GeChar = class('GeChar')

function GeChar:initialize(id, name, color, attributes)
	self.id = id
	self.name = name
	self.transname = be.transmanager:get(self.id)
	self.color = color
	self.attributes = {}
	if attributes then
		for key, value in pairs(attributes) do
			self[key] = value
			table.insert(self.attributes, key)
		end
	end
	-- self.use_in_panel = use_in_panel
end

function GeChar:getName()
	return self.transname
end

function GeChar:getAttributes()
	local attributes = ''
	for i, v in ipairs(self.attributes) do
		attributes = attributes .. be.transmanager:get('attribute_' .. v) .. ' ' ..  self[v]
		if i ~= #self.attributes then
			attributes = attributes .. ', '
		end
	end
	return attributes
end

return GeChar