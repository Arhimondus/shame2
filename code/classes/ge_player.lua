local class = require 'middleclass'
local utils = require 'utils'
local be = require 'bidons'

local GeChar = require 'classes.ge_char'

local GePlayer = class('GePlayer', GeChar)

function GePlayer:initialize(name, color)
	GeChar.initialize(self, 'Player', name, color, nil, true)
	self.player_name = nil
	self.items = {}
	self.marks = {}
	
	self.money = 0
	self.authority = 0
	self.gossip = 0
	
	self.attributes = { 'authority' }
end

--function GePlayer:money(to)
--	if to > 30 then
--		table.insert(be.context.notifications, { type = "money", color = "#FFCD7D", text = '+' .. to .. " денег" })
--	elseif to > 0 then
--		table.insert(be.context.notifications, { type = "money", color = "#FFCD1C", text = '+' .. to .. " денег" })
--	else
--		table.insert(be.context.notifications, { type = "money", color = "#FF7072", text = '-' .. to .. " денег" })
--	end
--	self._money = self._money + to
--	be.konstabel:demonstrate('notifications')
--end

--function GePlayer:authority(to)
--	if to > 0 then
--		table.insert(be.context.notifications, { type = "plus", color = "#FFCD1C", text = '+' .. to .. ' авторитет(а)' })
--	else
--		table.insert(be.context.notifications, { type = "money", color = "#FF7072", text = '-' .. to .. ' авторитета(а)' })
--	end
--	self._authority = self._authority + to
--	be.konstabel:demonstrate('notifications')
--end

function GePlayer:getName()
	return self.player_name or self.transname
end

function GePlayer:removeBdlItem(bdl_name)
	local item, index = self:fromBdlItem(bdl_name)
	if item then
		item.count = item.count - 1
		if item.count == 0 then
			table.remove(self.items, index)
		end
	end
end

function GePlayer:giveItem(item, count)
	count = count or 1
	table.insert(self.items, { item = item, count = count })
end

function GePlayer:hasItem(item, count)
	count = count or 1
	if utils.findFirst(self.items, function(v)
		return v.item == item and v.count >= count
	end) then
		return true
	else
		return false
	end
end

function GePlayer:giveBdlItem(bdl_name, count)
	count = count or 1
	local item = self:fromBdlItem(bdl_name)
	if item and item.item.stock then
		item.count = item.count + 1
	else
		for k, v in pairs(be.context.items) do
			if v.name == bdl_name then
				if v.stock then
					table.insert(self.items, { item = v, count = count })
				else
					for i = 1, count do
						table.insert(self.items, { item = v, count = 1 })
					end
				end
				do return end
			end
		end
	end
end

function GePlayer:hasBdlItem(bdl_name)
	for i, v in ipairs(self.items) do
		if v.item.name == bdl_name then
			return true
		end
	end
	return false
end

function GePlayer:fromBdlItem(bdl_name)
	for i, v in ipairs(self.items) do
		if v.item.name == bdl_name then
			return v, i
		end
	end
	return nil
end

return GePlayer