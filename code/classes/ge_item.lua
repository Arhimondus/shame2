local class = require 'middleclass'

local GeItem = class('GeItem')

function GeItem:initialize(id, name, type, color, stock)
	self.id = id
	self.name = name
	-- self.transname = be.transmanager:get(self.id)
	self.type = type
	self.color = color
	self.stock = stock
end

function GeItem:__index(property)
	if property == 'image' then
		return 'assets/items/' .. self.type .. '.png'
	else
		return rawget(self, property)
	end
end

return GeItem