local class = require 'middleclass'
local GeContext = require 'classes.ge_context'

local GeChapter = class('GeChapter')

function GeChapter:initialize()
	self.context = GeContext:new()
end

return GeChapter