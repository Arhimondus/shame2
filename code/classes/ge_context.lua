local class = require 'middleclass'
local utils = require 'utils'
local lume = require 'lume'
local be = require 'bidons'

local GeContext = class('GeContext')

function GeContext:initialize()
	self.day = 1
	self.daypart = 1
	self.chars = {}
	self.panel_chars = {}
	self.items = {}
	self.flags = {}
	self.notifications = {}
	
	self.DAYPARTS = {
		Morning = 1,
		Day = 2,
		Evening = 3,
		Night = 4
	}
	
	self.skip_after_choice = false
	
	be.context = self
end

function GeContext:__index(property)
	if self.chars[property] then
		return self.chars[property]
	else
		return rawget(self, property)
	end
end

function GeContext:gameVersion()
	return be.gameVersion
end

function GeContext:start(scene)
	self.state.registerEvents()
end

function GeContext:addChar(ge_char)
	self.chars[ge_char.id] = ge_char
	if ge_char.class.name == 'GePlayer' then
		self.player = ge_char
	end
	if ge_char.use_in_panel then
		table.insert(self.panel_chars, ge_char)
	end
end

function GeContext:addItem(ge_item)
	self.items[ge_item.id] = ge_item
end

function GeContext:bdlChar(bdl_name)
	for key, value in pairs(self.chars) do
		if value.name == bdl_name then
			return value
		end
	end
end

function GeContext:nextDayPart()
	self.daypart = self.daypart + 1
	if self.daypart > 4 then
		self.daypart = 1
		self:endDay()
		self.day = self.day + 1
	end
end

function GeContext:endDay()
	be.stacklog:info('End of day ' .. self.day .. ', next ' .. (self.day + 1))
end

function GeContext:skipDayParts(count)
	for i = 1, count do
		self:nextDayPart()
	end
end

function GeContext:isLightDayPart()
	return utils.between(self.daypart, 1, 2)
end

function GeContext:isDarkDayPart()
	return utils.between(self.daypart, 3, 4)
end

function GeContext:isMorning()
	return self.daypart == 1
end

function GeContext:isDay()
	return self.daypart == 2
end

function GeContext:isEvening()
	return self.daypart == 3
end

function GeContext:isNight()
	return self.daypart == 4
end

function GeContext:getDayPartName(daypart)
	if daypart == nil then
		daypart = be.context.daypart
	end
	if daypart == 1 then
		return be.transmanager:get('city_daypart_morning')
	elseif daypart == 2 then
		return be.transmanager:get('city_daypart_day')
	elseif daypart == 3 then
		return be.transmanager:get('city_daypart_evening')
	else
		return be.transmanager:get('city_daypart_night')
	end
end

function GeContext:getDayPartMnemo()
	if be.context.daypart == 1 then
		return 'morning'
	elseif be.context.daypart == 2 then
		return 'day'
	elseif be.context.daypart == 3 then
		return 'evening'
	else
		return 'night'
	end
end

function GeContext:flag(dialog, name)
	if not self.flags[dialog] then
		self.flags[dialog] = {}
	end
	
	local flag = utils.findFirst(self.flags[dialog], function(e) return e == name end)
	if not flag then
		table.insert(self.flags[dialog], name)
	end
end

function GeContext:checkFlag(dialog, name)
	if lume.find(self.flags[dialog], name) then
		return true
	else
		return false
	end
end


function GeContext:closeCityPanels()
	if self.inventory_panel then
		be.konstabel:disarm(self.inventory_panel)
	end
	if self.characters_panel then
		be.konstabel:disarm(self.characters_panel)
	end
	self.inventory_panel = nil
	self.characters_panel = nil
end

function GeContext:toggleInventoryPanel()
	if self.characters_panel then
		be.konstabel:disarm(self.characters_panel)
		self.characters_panel = nil
	end
	
	if self.inventory_panel then
		be.konstabel:disarm(self.inventory_panel)
		self.inventory_panel = nil
	else
		self.inventory_panel = be.konstabel:demonstrate('inventory')
	end
end

function GeContext:toggleCharactersPanel()
	if self.inventory_panel then
		be.konstabel:disarm(self.inventory)
		self.inventory_panel = nil
	end
	
	if self.characters_panel then
		be.konstabel:disarm(self.characters_panel)
		self.characters_panel = nil
	else
		self.characters_panel = be.konstabel:demonstrate('characters')
	end
end

function GeContext:getChars()
	local arr = {}
	for key, value in pairs(self.chars) do
		if key ~= 'Narrator' then
			table.insert(arr, value)
		end
	end
	return arr
end

return GeContext