be = require 'bidons'
utils = require 'utils'
flux = require 'flux'
timer = require 'timer'
Customer = require 'bordel.customers.base'
Box = require 'elements.ne-box'

bordel = {
	trans: be.transmanager,
	context: be.context,
	customers: {},
	total_count: 0
}

bordel.reverse = (tbl) =>
	for i=1, math.floor(#tbl / 2)
		tbl[i], tbl[#tbl - i + 1] = tbl[#tbl - i + 1], tbl[i]
	tbl

bordel.on_start = () =>

bordel.start = (scene) =>	
	@show_ctl = scene\single_bscn('bordel_tan')
	@girl_ctl = scene\single_bscn('bordel_girl')
	
	@girls = @context.allgirls -- заменить потот на allgirls на girls
	
	@scene = scene
	@customers_box = scene\get('customers')
	
	bordel.customers = {}
	@customers_box.objects = {}
	
	@girls_box = scene\get('girls')
	
	@all_customer_types = Customer\getAll()
	
	@total_count = 0
	
	@new_customer_timer = timer.every 0.5, (t) ->
		if @total_count >= 16 then
			timer\clear()
			be.konstabel\entrust('dialog', 'Бордель')
			return
		
		customer = @new_customer()
		@total_count += 1
		if not customer then return
		customer.box = Box\new(@scene, @customers_box.source.template, @customers_box.location, customer, #@customers, @customers_box)
		customer.flux = flux.to(customer.box.objects[2], 9, { width: 0 })\oncomplete () ->
			customer.box\selfdestroy!
		customer.box.xcord = @customers_box.width - #@customers * 160 -- 160
		@customers_box\add(customer.box)
		table.insert(@customers, customer)			
		-- customer.box\updx()
		
bordel.new_customer = () =>
	cs = fr(@all_customer_types)\reverse!\where((t) -> t.requirement <= @context.bordel_power)\select((t) -> {
		customer: t,
		random: love.math.random(100)
	})
	customer = cs\where((t) -> t.random < t.customer.chance)\first!

	if customer then customer.customer\new! else nil
	-- развернуть массив, сгенерить все вероятности появления, взять первый успешный

bordel.appoint = (customer, girl) =>
	print "appoint #{customer.name} to #{girl.name}!!!"
	customer.flux\stop!
	customer.box.objects[2].width = 0
	customer.girl = girl
	girl.customer = customer
	customer.flux = flux.to(customer.box.objects[2], 2, { width: 82 })\oncomplete () ->
		girl.customer = nil
		if customer.authority > 0
			@show(@trans\get('attribute_authority') .. ' +' .. customer.authority, customer.box.absolute, '#31a2ce')
			be.context.chars.Player.authority += customer.authority
		if customer.money > 0
			@show(@trans\get('attribute_money') .. ' +' .. customer.money, utils.offset(customer.box.absolute, { 0, 30 }), '#e0cb6d')
			be.context.chars.Player.money += customer.money
		if customer.bordel_power > 0
			@show(@trans\get('bordel_power') .. ' +' .. customer.bordel_power, utils.offset(customer.box.absolute, { 0, 60 }), '#ffaca3')
			be.context.bordel_power += customer.bordel_power
		customer.box\selfdestroy!
		
	
bordel.test70 = () =>
	cbg = @scene\get('customer_bg#0')
	cbg\change_owner(cbg.owner.owner)

bordel.show = (text, position, color) =>
	tan = @scene\loadElement(@show_ctl, position, text, 1, @scene.objects[1])
	tan.absolute = position
	tan.color = color
	@scene.objects[1]\add(tan)
	flux.to(tan, 3, { opacity: 1, ycord: tan.absy - 100 })\ease('expoout')\oncomplete () ->
		tan\selfdestroy!
	
bordel