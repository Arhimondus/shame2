local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Citizen = class('Citizen', Customer)

Citizen.static.requirement = -200
Citizen.static.chance = 90

function Citizen:initialize()
	Customer.initialize(self,
		Customer.grades.citizen,
		self:random(20), -- Золото
		0, -- Влияние игроку после обслуживания
		10, -- Сколько выдаётся авторитета борделю после обслуживания
		-200, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic }
	)
end

return Citizen