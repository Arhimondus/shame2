local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Fraulein = class('Fraulein', Customer)

Fraulein.static.requirement = 7000
Fraulein.static.chance = 40

function Fraulein:initialize()
	Customer.initialize(self,
		Customer.grades.fraulein,
		self:random(60), -- Золото
		2, -- Влияние игроку после обслуживания
		50, -- Сколько выдаётся авторитета борделю после обслуживания
		7000, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.lesbian,
			Customer.services.cunnilingus,
			Customer.services.strapon,
			Customer.services.slave,
			Customer.services.fisting,
			Customer.services.spanking,
			Customer.services.rimming }
	)
end

return Fraulein