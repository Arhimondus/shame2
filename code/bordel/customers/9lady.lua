local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Lady = class('Lady', Customer)

Lady.static.requirement = 10000
Lady.static.chance = 20

function Lady:initialize()
	Customer.initialize(self,
		Customer.grades.lady,
		self:random(100), -- Золото
		10, -- Влияние игроку после обслуживания
		200, -- Сколько выдаётся авторитета борделю после обслуживания
		10000, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.anal,
			Customer.services.deep_throating,
			Customer.services.lesbian,
			Customer.services.cunnilingus,
			Customer.services.strapon,
			Customer.services.mistress,
			Customer.services.slave,
			Customer.services.spanking,
			Customer.services.fisting,
			Customer.services.rimming }
	)
end

return Lady