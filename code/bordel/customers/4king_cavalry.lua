local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local KingCavalry = class('KingCavalry', Customer)

KingCavalry.static.requirement = 3000
KingCavalry.static.chance = 60

function KingCavalry:initialize()
	Customer.initialize(self,
		Customer.grades.king_cavalry,
		self:random(50), -- Золото
		1, -- Влияние игроку после обслуживания
		30, -- Сколько выдаётся авторитета борделю после обслуживания
		3000, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.anal,
			Customer.services.deep_throating }
	)
end

return KingCavalry