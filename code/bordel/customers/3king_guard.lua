local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local KingGuard = class('KingGuard', Customer)

KingGuard.static.requirement = 1200
KingGuard.static.chance = 70

function KingGuard:initialize()
	Customer.initialize(self,
		Customer.grades.king_guard,
		self:random(40), -- Золото
		1, -- Влияние игроку после обслуживания
		20, -- Сколько выдаётся авторитета борделю после обслуживания
		1200, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.anal }
	)
end

return KingGuard