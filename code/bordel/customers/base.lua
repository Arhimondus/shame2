local class = require 'middleclass'
local be = require 'bidons'
local Customer = class('Customer')

Customer.static.grades = {
	citizen = 1,
	guard = 2,
	king_guard = 3,
	king_cavalry = 4,
	courtier = 5,
	fraulein = 6,
	commander = 7,
	lord = 8,
	lady = 9
}

Customer.static.services = {
	striptease = 1,
	massage = 2,
	classic = 3,
	anal = 4,
	deep_throating = 5,
	lesbian = 6,
	cunnilingus = 7,
	strapon = 8,
	mistress = 9,
	slave = 10,
	spanking = 11,
	fisting = 12,
	rimming = 13
}

function Customer.static:getAll()
	local a = {}
	table.insert(a, require('bordel.customers.1citizen'))
	table.insert(a, require('bordel.customers.2guard'))
	table.insert(a, require('bordel.customers.3king_guard'))
	table.insert(a, require('bordel.customers.4king_cavalry'))
	table.insert(a, require('bordel.customers.5courtier'))
	table.insert(a, require('bordel.customers.6fraulein'))
	table.insert(a, require('bordel.customers.7commander'))
	table.insert(a, require('bordel.customers.8lord'))
	table.insert(a, require('bordel.customers.9lady'))
	return a
end

function Customer:initialize(grade, money, authority, bordel_power, bordel_power_require, services)
	self.grade = grade
	self.money = money
	self.authority = authority
	self.bordel_power = bordel_power
	self.bordel_power_require = bordel_power_require
	
	self.name = string.lower(self.class.name)
end

function Customer:random(x)
	return math.floor(love.math.random(x * 0.85, x * 1.15))
end

return Customer