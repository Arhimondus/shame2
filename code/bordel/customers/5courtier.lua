local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Courtier = class('Courtier', Customer)

Courtier.static.requirement = 5000
Courtier.static.chance = 50

function Courtier:initialize()
	Customer.initialize(self,
		Customer.grades.courtier,
		self:random(60), -- Золото
		2, -- Влияние игроку после обслуживания
		50, -- Сколько выдаётся авторитета борделю после обслуживания
		5000, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.anal,
			Customer.services.deep_throating,
			Customer.services.mistress }
	)
end

return Courtier