local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Guard = class('Guard', Customer)

Guard.static.requirement = 300
Guard.static.chance = 80

function Guard:initialize()
	Customer.initialize(self,
		Customer.grades.guard,
		self:random(30), -- Золото
		0, -- Влияние игроку после обслуживания
		20, -- Сколько выдаётся авторитета борделю после обслуживания
		300, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.deep_throating }
	)
end

return Guard