local class = require 'middleclass'
local be = require 'bidons'
local Customer = require 'bordel.customers.base'

local Commander = class('Commander', Customer)

Commander.static.requirement = 8000
Commander.static.chance = 30

function Commander:initialize()
	Customer.initialize(self,
		Customer.grades.commander,
		self:random(70), -- Золото
		5, -- Влияние игроку после обслуживания
		100, -- Сколько выдаётся авторитета борделю после обслуживания
		8000, -- Требование по авторитету, проверяется при приходе
		{ Customer.services.striptease,
			Customer.services.massage,
			Customer.services.classic,
			Customer.services.anal,
			Customer.services.deep_throating,
			Customer.services.mistress,
			Customer.services.slave,
			Customer.services.spanking,
			Customer.services.fisting,
			Customer.services.rimming }
	)
end

return Commander