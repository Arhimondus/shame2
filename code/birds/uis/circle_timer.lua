local be = require 'bidons'

return function(x, y, r)
	local circle_timer = {}
	circle_timer.rx = x
	circle_timer.ry = y
	circle_timer.radius = r
	circle_timer.thickness = 10
	circle_timer.angle = 0
	circle_timer.showed = 0
	circle_timer.font = love.graphics.newFont('assets/fonts/open_sans.ttf', 40)

	circle_timer.type = "ui_circle_timer"
	circle_timer.stencil = function()
		love.graphics.arc( "fill", circle_timer.rx, circle_timer.ry, circle_timer.radius + 1, math.rad(-90), math.rad(-90 - circle_timer.angle))
		love.graphics.circle("fill", circle_timer.rx, circle_timer.ry, circle_timer.radius - circle_timer.thickness)	
	end
	circle_timer.draw = function()
		love.graphics.setColor(1, 1, 1)
		
		love.graphics.stencil(circle_timer.stencil, "increment")
		love.graphics.setStencilTest("less", 1)
		love.graphics.circle("fill", circle_timer.rx, circle_timer.ry, circle_timer.radius)
		love.graphics.setStencilTest()
		
		love.graphics.setFont(circle_timer.font)
		love.graphics.print(circle_timer.showed, circle_timer.rx - circle_timer.font:getWidth(circle_timer.showed) / 2 - 1, circle_timer.ry - circle_timer.font:getHeight(circle_timer.showed) / 2)
	end

	circle_timer.start = function(time)
		circle_timer.total = time
		circle_timer.elapsed = time
		circle_timer.showed = time
		circle_timer.step = time / 360
		circle_timer.angle = 0
		
		be.timer.every(circle_timer.step, function() 
			circle_timer.angle = circle_timer.angle + 1
			circle_timer.elapsed = circle_timer.total - circle_timer.angle * circle_timer.step
			circle_timer.showed = math.ceil(circle_timer.elapsed)
			if circle_timer.angle == 360 then
				be.timer.clear()
			end
		end)
	end

	circle_timer.stop = function()
		be.timer.clear()
	end

	return circle_timer
end