local ui = require 'dialog.ui'
local fd = require 'fastdraw'
local utils = require 'utils'

return function(b)
	b.type = "option_button"
	b.fontSize = 18
	b.font = assets.fonts.open_sans(b.fontSize)
	b.text_height_offset = 0
	b.patron_enabled = b.text:sub(1, 1) == '@'
	
	if b.patron_enabled then
		b.text = b.text:sub(2)
	end
	while b.position[3] - 20 < b.font:getWidth(b.text) do
		b.fontSize = b.fontSize - 1
		b.font = assets.fonts.open_sans(b.fontSize)
		b.text_height_offset = b.text_height_offset + 1
	end
	b.mousemoved = function(_, x, y)
		b.is_hovered = utils.inside_rectangle_o(x, y, b.position)
	end
	b.draw = function()
		if not b.disabled then
			fd.color().draw(not b.is_hovered and assets.option or assets.option_selected, b.position[1] - 15, b.position[2])
		else
			fd.color().draw(assets.option_disabled, b.position[1] - 15, b.position[2])
		end
	
		fd.color(not b.disabled and "#000000" or "#5B5B5B").font(b.font).print(b.text, b.position[1] + 9, b.position[2] + 2 + b.text_height_offset)
		
		if b.disabled then
			if not b.patron_enabled then
				fd.color("#635736").draw(assets.lock, unpack(b.lock_position))
			else
				fd.color().draw(assets.lock_patreon, unpack(b.lock_position))
			end
		end
		
		if b.repeat_icon then
			fd.color("#006600ff").draw(assets.items['repeat'], b.position[1] + 370, b.position[2] + 3)
		end
	end
	b.mousepressed = function(_, x, y, button)
		if button == 1 and b.is_hovered then
			b.click()
			return true
		end
	end
	return b
end