local ui = require 'dialog.ui'
local fd = require 'fastdraw'
local utils = require 'utils'

return function(b)
	b.type = "small_option_button"
	b.fontSize = 18
	b.font = assets.fonts.open_sans(b.fontSize)
	b.text_height_offset = 0
	
	while b.position[3] - 20 < b.font:getWidth(b.text) do
		b.fontSize = b.fontSize - 1
		b.font = assets.fonts.open_sans(b.fontSize)
		b.text_height_offset = b.text_height_offset + 1
	end
	
	b.text_width_offset = (b.position[3] - b.font:getWidth(b.text)) / 2 - 13
		
	b.mousemoved = function(_, x, y)
		b.is_hovered = utils.point_inside_rect_xywh(x, y, b.position)
	end
	b.draw = function()
		if not b.disabled then
			fd.color().draw(not b.is_hovered and assets.small_option or assets.small_option_selected, b.position[1] - 15, b.position[2])
		else
			fd.color().draw(assets.small_option_disabled, b.position[1] - 15, b.position[2])
		end
	
		fd.color(not b.disabled and "#000000" or "#5B5B5B").font(b.font).print(b.text, b.position[1] + b.text_width_offset, b.position[2] + 2 + b.text_height_offset)
		
		if b.disabled then
			fd.color("#635736").draw(assets.lock, unpack(b.lock_position))
		end
		
		be.color()
		-- love.graphics.rectangle("line", unpack(b.position))
	end
	b.mousepressed = function(_, x, y, button)
		if button == 1 and b.is_hovered then
			b.click()
			return true
		end
	end
	return b
end