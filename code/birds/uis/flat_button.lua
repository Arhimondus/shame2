local fd = require 'fastdraw'
local utils = require 'utils'

return function(b)
	b.type = "flat_button"
	b.rposition = { b.position[1], b.position[2], b.font:getWidth(b.text),  b.font:getHeight() }
	b.draw = function()
		fd.color(b.is_hovered and b.hovered_color or b.text_color).font(b.font).print(b.text, unpack(b.position))
	end
	b.mousemoved = function(_, x, y)
		b.is_hovered = utils.inside_rectangle_o(x, y, b.rposition)
	end
	b.mousepressed = function(_, x, y, button)
		if(utils.inside_rectangle_o(x, y, b.rposition)) then
			b.click()
			return true
		end
	end
	return b
end