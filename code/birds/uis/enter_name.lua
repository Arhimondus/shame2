local utf8 = require "utf8"
local fd = require 'fastdraw'
local utils = require 'utils'

return function(callback)
	local enb = {}
	enb.type = "enter_name"
	enb.text = ''
	enb.title = be.transmanager:get('dialog_chooseyourname') 
	enb.callback = callback
	enb.draw = function()
		fd.color("black", "99").rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
		fd.color().font("game_of_thrones_kg", 32).printc(enb.title, love.graphics.getWidth() / 2, love.graphics.getHeight() / 2 - 100)
		fd.color().font("pt_astra_serif_regular", 40).printc(enb.text, love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
	end
	enb.mousemoved = function(_, x, y)
	end
	enb.mousepressed = function(_, x, y, button)
		return true
	end
	enb.keypressed = function(_, key, sc)
		if key == "backspace" then
			local byteoffset = utf8.offset(enb.text, -1)
 
			if byteoffset then
				-- remove the last UTF-8 character.
				-- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
				enb.text = string.sub(enb.text, 1, byteoffset - 1)
			end
		elseif key == "return" then
			enb.callback(enb.text)
		end
	end
	enb.textinput = function(_, key)
		if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then
			enb.text = enb.text .. key:upper()
		else
			enb.text = enb.text .. key
		end
	end
	enb.keyreleased = function()
		return true -- Просто блокируем остальные нажатия клавиш
	end
	return enb
end