local ui = require 'birds.ui'
local lume = require 'lume'
local utils = require 'utils'
local fd = require 'fastdraw'
local be = require 'bidons'

local birds = {}

function birds:init()
	ui.init(birds)
	birds.timer = ui.add(birds, "circle_timer", 1280 / 2, 50, 50)
end

function birds:enter()
	-- 10 / 5
	birds.list = {}
	for i = 1, 20 do
		table.insert(birds.list, {
			position = { lume.random(0, 1280 / 120 - 1) * 120, lume.random(0, 720 / 103 - 1) * 103 },
			is_death = false
		})
	end
	birds.list[#birds.list].proper = true
	
	birds.list = lume.shuffle(birds.list)
	
	birds.elapsed = #birds.list
	
	birds.timer.start(5)
	
	birds.bfont = love.graphics.newFont('assets/fonts/game_of_thrones_kg.ttf', 24)
	birds.mfont = love.graphics.newFont('assets/fonts/game_of_thrones_kg.ttf', 20)
	birds.sfont = love.graphics.newFont('assets/fonts/open_sans.ttf', 18)
	
	birds.background = love.graphics.newImage('assets/backgrounds/Таверна.jpg')
	birds.bird = love.graphics.newImage('assets/bird.png')
end

function birds:draw()
	love.graphics.draw(birds.background, 0, 0)
	love.graphics.setColor(1, 1, 1, 1)
	
	for i, r in ipairs(birds.list) do
		if not r.is_death then
			love.graphics.draw(birds.bird, unpack(r.position))
		end
	end
	
	-- local elapsed = lume.count(birds.list, { is_death = false })
	if birds.elapsed ~= 0 and birds.timer.showed > 0 then
		fd.font(birds.bfont).printc(be.transmanager:get('birds_left') .. ' ' .. birds.elapsed, love.graphics.getWidth() / 2, 120)
	elseif birds.elapsed == 0 and birds.timer.showed > 0 then
		fd.font(birds.bfont).printc(be.transmanager:get('birds_done'), love.graphics.getWidth() / 2, 120)
		fd.font(birds.sfont).printc(be.transmanager:get('birds_pressesc'), love.graphics.getWidth() / 2, 150)
	else
		fd.font(birds.mfont).printc(be.transmanager:get('birds_shame'), love.graphics.getWidth() / 2, 120)
		fd.font(birds.sfont).printc(be.transmanager:get('birds_pressesc'), love.graphics.getWidth() / 2, 150)
	end
	
	ui.draw(birds)
end

function birds:mousepressed(x, y)
	if birds.timer.showed > 0 then
		for i, r in lume.ripairs(birds.list) do
			if not r.is_death and utils.inside_rectangle_o(x, y, { r.position[1], r.position[2], 256, 207 }) then
				r.is_death = true
				birds.elapsed = lume.count(birds.list, { is_death = false })
				if birds.elapsed == 0 then
					be.timer.clear()
				end
				return
			end
		end
	end
end

function birds:keyreleased(key, code)
    if key == "escape" then
		if birds.elapsed == 0 and birds.timer.showed > 0 then
			be.context.Player.gossip = be.context.Player.gossip + 3
		end
		be.timer.clear()
		be.konstabel:entrust('dialog', 'Таверна')
    end
end

function birds:update(dt)
	be.timer.update(dt)
end

return birds