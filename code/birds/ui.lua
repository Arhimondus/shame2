local lume = require 'lume'

local ui = {}

function ui.init(state)
	state.ui = state.ui or {}
	state.ui.objects = state.ui.objects or {}
end

function ui.add(state, type, ...)
	local obj = require("birds.uis." .. type)(...)
	table.insert(state.ui.objects, obj)
	return obj
end

function ui.remove(state, uie)
	local uie_index = lume.find(state.ui.objects, uie)
	if uie_index ~= nil then
		table.remove(state.ui.objects, uie_index)
	end
end

function ui.findFirstType(state, type)
	return utils.findFirst(state.ui.objects, function(e) return e.type == type end)
end

function ui.draw(state)
	love.graphics.setDefaultFilter("nearest", "nearest")
	for _, o in ipairs(state.ui.objects) do
		if o.draw then o:draw() end
	end
end

function ui.mousemoved(state, x, y)
	for _, o in ipairs(state.ui.objects) do
		if o.mousemoved then o:mousemoved(x, y) end
	end
end

function ui.mousepressed(state, x, y, button)
	for _, o in ipairs(state.ui.objects) do
		if o.mousepressed then 
			if o:mousepressed(x, y, button) then
				return true
			end
		end
	end
end

function ui.keypressed(state, key, sc)
	for _, o in ipairs(state.ui.objects) do
		if o.keypressed then o:keypressed(key, sc) end
	end
end

function ui.keyreleased(state, key)
	for _, o in ipairs(state.ui.objects) do
		if o.keyreleased then 
			if o:keyreleased(key) then
				return true
			end
		end
	end
end

function ui.textinput(state, text)
	for _, o in ipairs(state.ui.objects) do
		if o.textinput then o:textinput(text) end
	end
end

return ui
