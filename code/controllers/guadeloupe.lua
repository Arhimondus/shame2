local flux = require 'flux'
local wae = require 'wae'
local utils = require 'utils'
local be = require 'bidons'
local class = require 'middleclass'

local guadeloupe =  class('Guadeloupe')

function guadeloupe:initialize()
	print('GUADE Initiliaze')
	self.float_damage = 0
	self.hero_float_damage = 0
	self.game_started = false
	self.step_arrow_pos = { 381, 147 }
end

function guadeloupe:on_start(scene)
	print('GUADE on_start')
	self.scene = scene
	self.round = 1
	self.label = scene:get('float_damage')
	self.hlabel = scene:get('hero_float_damage')
	self.step = nil -- Номер игрока, чей ход 1 - низ, 2 - верх
	self.max_rage_value = 50
end

function guadeloupe:destroy(card, owner)
	local array
	if owner == 1 then
		array = be.context.guadeloupe.bottom.units
	else
		array = be.context.guadeloupe.top.units
	end
	utils.remove(array, function(e) return e == card end)
	
	self.scene:get('units_top'):updateBinding()
	self.scene:get('units_bottom'):updateBinding()
end

function guadeloupe:damage(element, value)
	local label
	if element.objname == 'hero_top' or element.objname == 'hero_bottom' then
		label = self.hlabel
		self.hero_float_damage = value	
	else
		label = self.label
		self.float_damage = value	
	end
	
	label.opacity = 1
	label.location = { element.absolute[1] + element.size[1] / 2, element.absolute[2] }
	
	element.binding_object.health = element.binding_object.health - value
	
	local anim = flux.to(label, 1, { opacity = 0, ycord = label.absolute[2] - 80 })
	:ease('linear')
	
	if element.binding_object.health <= 0 then
		-- Наносим урон герою
		local hero_damage = -1 * element.binding_object.health
		
		if hero_damage > 0 then
--			anim:oncomplete(function(e)
				local hero
				if element.binding_object.owner == 1 then
					hero = self.scene:get('hero_bottom')
				else
					hero = self.scene:get('hero_top')
				end
				
				self:damage(hero, hero_damage)
--			end)
		end
	
		utils.remove(element.owner.array, function(e) return e == element.binding_object end)
		element.owner:updateBinding()
		
		self:run_single(element.binding_object, 'card_selfdestruct', nil)
		self:run_all('card_destruct', { initiator = element.binding_object })
	end
	
	self:updateAll()
end

--function guadeloupe:__index(property)
--	if property == 'absolute' then
--		return 10
--	else
--		return rawget(self, property)
--	end
--end

--function guadeloupe:action()
--	assert(loadstring('return function(self) ' .. v.action .. ' end'))(self)
--end

--function guadeloupe.compute_attack(card, owner, opponent)
--	for i, v in ipairs(self:units(owner)) do
--		if v.compute_attack then
--		assert(loadstring('return function(self, card, owner, opponent) ' .. v.compute_attack .. ' end'))(self, card, owner, opponent)
--		end
--	end
--	for i, v in ipairs(self:units(opponent)) do
--		assert(loadstring('return function(self, card, owner, opponent) ' .. v.compute_attack .. ' end'))(self, card, owner, opponent)
--	end
--end

function guadeloupe:units(owner_id)
	if owner_id == 1 then
		return from(be.context.guadeloupe.bottom.units)
	elseif owner_id == 2 then
		return from(be.context.guadeloupe.top.units)
	end
end

function guadeloupe:action_damage(attacker, defender, damage)
	if damage == nil then
		damage = attacker.damage
	end
end

-- 215 - 100%, max_rage_value = 50	
-- Просто изменение
-- rbc.size[2] = math.floor(bottomrage * 215 / max_rage_value)
-- rbb.location[2] = rbc.size[2] - 215
-- rtc.size[2] = math.floor(anim_top.value * 215 / max_rage_value)
	
function guadeloupe:change_top_rage(value)
	-- Ярость верх
	local rtc = self.scene:get('rage_top_container')
	local rtb = self.scene:get('rage_top_box')
	
	local old_toprage = be.context.guadeloupe.top.rage
	be.context.guadeloupe.top.rage = be.context.guadeloupe.top.rage + value
	if be.context.guadeloupe.top.rage > self.max_rage_value then
		be.context.guadeloupe.top.rage = self.max_rage_value
	end
	
	-- Ярость верх - анимация
	local anim_top = { value = old_toprage }
	flux.to(anim_top, 0.5, { value = be.context.guadeloupe.top.rage })
	:onupdate(function()
		rtc.size[2] = math.floor(anim_top.value * 215 / self.max_rage_value)
		-- rbb.location[2] = rbc.size[2] - 215
	end)
	:ease('expoout')
	
	self.scene:get('rage_top_count'):updateSize()
end

function guadeloupe:change_bottom_rage(value)
	-- Ярость низ
	local rbc = self.scene:get('rage_bottom_container')
	local rbb = self.scene:get('rage_bottom_box')
	
	local old_bottomrage = be.context.guadeloupe.bottom.rage
	be.context.guadeloupe.bottom.rage = be.context.guadeloupe.bottom.rage + value
	if be.context.guadeloupe.bottom.rage > self.max_rage_value then
		be.context.guadeloupe.bottom.rage = self.max_rage_value
	end
	
	-- Ярость низ - анимация
	local anim_bottom = { value = old_bottomrage }
	flux.to(anim_bottom, 0.5, { value = be.context.guadeloupe.bottom.rage })
	:onupdate(function()
		rbc.size[2] = math.floor(anim_bottom.value * 215 / self.max_rage_value)
		rbb.location[2] = rbc.size[2] - 215
	end)
	:ease('expoout')
	
	self.scene:get('rage_bottom_count'):updateSize()
end

function guadeloupe:drop_to_bottom(param)	
	if not self.game_started then return end
	
	local attacker = param.object.binding_object
	local defender = param.drop_element.binding_object
	
	print(attacker.name .. ' attacks ' .. defender.name .. ' with ' .. (attacker.attack + attacker.bonus_attack) .. ' damage!')
	self:damage(param.drop_element, attacker.attack + attacker.bonus_attack)
	
	if attacker.owner == 1 then
		self:change_bottom_rage(1)
	elseif attacker.owner == 2 then
		self:change_top_rage(1)
	end

	self:next_turn()
	
	return false
end

function guadeloupe:next_turn()
	if self.step == 1 then
		self.step = 2
	elseif self.step == 2 then
		self:run_all('round_end')		
		self.round = self.round + 1
		self.step = 1
		self:run_all('round_start')
		
		-- Начало нового раунда, добавляем по карте, если меньше карт меньше 4
		local deck_top = self.scene:get('deck_top')
		local deck_bottom = self.scene:get('deck_bottom')
		local units_top = self.scene:get('units_top')
		local units_bottom = self.scene:get('units_bottom')
					
		if #be.context.guadeloupe.top.units < 4 then
			local ar = be.context.chars.Narrator.cards
			local el = from(ar):random()
			utils.remove(ar, function(e) return e == el end)
			table.insert(be.context.guadeloupe.top.units, el)
			
			local xcord = units_top.xcord + (#be.context.guadeloupe.top.units - 1) * units_top.offset[1]
			deck_top.objects[1]:dragstart('drag')
			flux.to(deck_top.objects[1], 2, {
				xcord = xcord,
				ycord = units_top.ycord
			}):oncomplete(function(e)
				deck_top:updateBinding()
				units_top:updateBinding()
			end)
		end
	
		if #be.context.guadeloupe.bottom.units < 4 then
			local ar = be.context.chars.Player.cards
			local el = from(ar):random()
			utils.remove(ar, function(e) return e == el end)
			table.insert(be.context.guadeloupe.bottom.units, el)
			
			local xcord = units_bottom.xcord + (#be.context.guadeloupe.bottom.units - 1) * units_bottom.offset[1]
			deck_bottom.objects[1]:dragstart('drag')
			flux.to(deck_bottom.objects[1], 2, {
				xcord = xcord,
				ycord = units_bottom.ycord
			}):oncomplete(function(e)
				deck_bottom:updateBinding()
				units_bottom:updateBinding()
			end)
		end
	end
	
	local step_arrow = self.scene:get('step_arrow')
	flux.to(step_arrow, 0.5, { ycord = self.step_arrow_pos[self.step] })
	:ease('linear')
end

function guadeloupe:run_all(action, event)
	print('Action = ' .. action)
	self:units(1):foreach(function(card)
		print('> card ' .. card.name .. ' owner bot')
		if card[action] then
			print(card)
			print(self)
			print(event)
			card[action](card, self, event)
		end	
	end)
	self:units(2):foreach(function(card)
		print('> card ' .. card.name .. ' owner top')
		if card[action] then
			print(card)
			print(self)
			print(event)
			card[action](card, self, event)
		end	
	end)
	self:updateAll()
end

function guadeloupe:run_single(card, action, event)
	print('Single action = ' .. action .. ' owner: ' .. card.owner .. ' opponent: ' .. card.opponent)
	if card[action] then
		print('> card ' .. card.name)
		card[action](card, self, event)
	end
	self:updateAll()
end

function guadeloupe:drop_after_action(param)
	if not self.game_started then
		if param then
			local card = param.object.binding_object
			if param.drop_element.objname == 'units_top' then
				card.owner = 2
				card.opponent = 1
				card.bonus_attack = 0
			else
				card.owner = 1
				card.opponent = 2
				card.bonus_attack = 0
			end
		end
		if #be.context.guadeloupe.bottom.units == 4 and #be.context.guadeloupe.top.units == 4 then
			self:game_start()
		end
	else
		if param then
			local card = param.object.binding_object
			if param.drop_element.objname == 'units_top' then
				card.owner = 2
				card.opponent = 1
				card.bonus_attack = 0
			else
				card.owner = 1
				card.opponent = 2
				card.bonus_attack = 0
			end
			self:run_single(card, 'card_init', card.owner, card.opponent)
--			self:run_single(card, 'before', card.owner, card.opponent)
		end
	end
end

function guadeloupe:updateAll()
	for i, v in ipairs(self.scene:get('units_top').objects) do
		for j, d in ipairs(v.objects) do
			d:updateSize()
		end
	end
	
	for i, v in ipairs(self.scene:get('units_bottom').objects) do
		for j, d in ipairs(v.objects) do
			d:updateSize()
		end
	end
end

function guadeloupe:game_start()
	self.game_started = true
	
	print 'game_started!'
	
--	self:units(1):foreach(function(card)
--		card.owner = 1
--		card.opponent = 2
--		card.bonus_attack = 0
--	end)

--	self:units(2):foreach(function(card)
--		card.owner = 2
--		card.opponent = 1
--		card.bonus_attack = 0
--	end)
	
	self.step = 1 -- Первый ходит нижний игрок
	
	self:run_all('game_start')
	self:run_all('card_init')
	-- self:run_all('card_out')
	
	local db = self.scene:get('deck_bottom')
	local dt = self.scene:get('deck_top')
	local bwidth = utils.round(db:width() / 2)
	local twidth = utils.round(dt:width() / 2)
	
	db.anchor = {0, 0}
	dt.anchor = {0, 0}
	db.location[1] = db.location[1] - bwidth
	dt.location[1] = dt.location[1] - twidth
	
	self:updateAll()
	
	flux.to(db, 1, { xcord = 340 }):ease('expoout')
	flux.to(dt, 1, { xcord = 340 }):ease('expoout')
	
--	print(bwidth)
--	print(twidth)
end

return guadeloupe:new()