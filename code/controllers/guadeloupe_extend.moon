utils = require 'utils'

events = {
	game_start: {}
	card_selfdestruct: {} -- Карта сам уничтожается
	card_destruct: {} -- После уничтожения
	card_init: {} -- Выход конкретно текущей карты
	card_out: {} -- Выход любой карты
	card_before_damage: {}
	card_after_damage: {}
	card_can_attack: { default: (game, card) => self.attack > 0 }
	card_custom_attack: {}
	round_start: {}
	round_end: {}
	playerstep_before: {}
	playerstep_after: {}
	game_end: {}
}

cards = {
	ariya_stark: {
		-- Base
		id: 'uy938fgq',
		type: 'iijqtm761',
		name: 'Ария Старк',
		icon: 'assets/cards/norden/Arja.jpg',
		ztags: { '1jqza214' },
		support: false,
		
		-- Ingame
		attack: 0, health: 18,
		
		round_end: (game) => 
			self.step += 1
			if self.step > 3 then self.attack = 1000,
			
		-- Extend
		step: 1
	}
	blackfish: {
		-- Base
		id: 'pgnsufuc',
		type: 'iijqtm761',
		name: 'Чёрная рыба',
		icon: 'assets/cards/norden/Tally.jpg',
		ztags: { '1jqza214' },
		support: true,
		
		-- Ingame
		attack: 0, health: 25,
		
		round_end: (game) =>
			self.step += 1
			if self.step > 5 then game\destroy card,
		
		card_before_damage: (game, event) => --[[ event: attacker, defender, potential_damage --]] 
			if event.defender.owner == owner then event.potential_damage -= 2,
			
		-- Extend
		step: 1
	}
	boltons: {
		-- Base
		id: 'o644bgiw',
		type: 'iijqtm761',
		name: 'Болтоны',
		icon: 'assets/cards/norden/Boltons.jpg',
		ztags: { '1jqza214' },
		support: false,
		
		-- Ingame
		attack: 5, health: 20,
		
		playerstep_after: (game, event) =>  --[[ event: player --]]
			self.attack += 2,
		
		-- Extend
		step: 1
	}
	bran_stark: {
		-- Base
		id: '7223mf0w',
		type: 'iijqtm761',
		name: 'Бран Старк',
		icon: 'assets/cards/norden/Bran.jpg',
		ztags: { '1jqza214' },
		support: true,
		
		-- Ingame
		attack: 0, health: 5
	}
	bronn: {
		-- Base
		id: 'y8ommjcc',
		type: 'iijqtm761',
		name: 'Бронн',
		icon: 'assets/cards/regius/Bronn.jpg',
		ztags: { 'fa9442c6' },
		support: true,
		
		-- Ingame
		attack: 7, health: 13
	}
	dog: {
		-- Base
		id: 'y8ommjcc',
		type: 'iijqtm761',
		name: 'Пёс',
		icon: 'assets/cards/regius/Dog.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 15, health: 9
	}
	euron: {
		-- Base
		id: 'pbhzgpjp',
		type: 'iijqtm761',
		name: 'Эурон',
		icon: 'assets/cards/regius/Euron.jpg',
		ztags: { 'fa9442c6' },
		support: true,
		
		-- Ingame
		attack: 10, health: 34
	}
	hodor: {
		-- Base
		id: 's8utcua4',
		type: 'iijqtm761',
		name: 'Ходар',
		icon: 'assets/cards/norden/Hodor.jpg',
		ztags: { '1jqza214' },
		support: true,
		
		-- Ingame
		attack: 15, health: 20,
		
		card_init: (game) =>
			if not game\units(self.owner)\any (unit) -> unit.name == 'Бран Старк'
				self.attack = 0,
		
		card_destruct: (game, event) => self\card_init(game, event)
	}
	jayme: {
		-- Base
		id: 'p0sv1avm',
		type: 'iijqtm761',
		name: 'Jayme',
		icon: 'assets/cards/regius/Jayme.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 15, health: 19
	}
	joffry: {
		-- Base
		id: 'p0sv1avm',
		type: 'iijqtm761',
		name: 'Joffry',
		icon: 'assets/cards/regius/Joffry.jpg',
		ztags: { 'fa9442c6' },
		support: true,
		
		-- Ingame
		attack: 0, health: 4
	}
	loup_garou: {
		-- Base
		id: '4wqympqk',
		type: 'iijqtm761',
		name: 'Лютоволки',
		icon: 'assets/cards/norden/Wolves.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 5, health: 15,
		
		card_init: (game, event) =>
			game\units(self.owner)\any (unit) ->
				unit.tags\any (tag) ->
					tag == 'starks',
		
		card_destruct: (game, event) => self\card_init(game, event)
	}
	meera_reed: {
		-- Base
		id: 'bokbaqya',
		type: 'iijqtm761',
		name: 'Мира Рид',
		icon: 'assets/cards/norden/Mira.jpg',
		ztags: { '1jqza214' },
		support: false,
		
		-- Ingame
		attack: 4, health: 12
	}
	mountain: {
		-- Base
		id: 'jd8qy29y',
		type: 'iijqtm761',
		name: 'Mountain',
		icon: 'assets/cards/regius/Mountain.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 20, health: 27
	}
	qyburn: {
		-- Base
		id: 'a8vzfzoy',
		type: 'iijqtm761',
		name: 'Qyburn',
		icon: 'assets/cards/regius/Qyburn.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 10, health: 12
	}
	robb_stark: {
		-- Base
		id: 'fnphrv7h',
		type: 'iijqtm761',
		name: 'Robb Stark',
		icon: 'assets/cards/norden/Robb.jpg',
		ztags: { '1jqza214' },
		support: true,
		
		-- Ingame
		attack: 0, health: 10,
		
		card_init: (game, event) => game\units(self.owner)\foreach (unit) ->
			unit.bonus_attack = utils.round(unit.attack * 0.5),
		card_out: (game, event) => card_init(game, event)
		
		round_end: (game, event) =>
			self.step += 1
			if self.step == 2 then game\destroy(self),
			
		card_selfdestruct: (game, event) =>
			game\units(self.owner)\foreach (unit) ->
				unit.bonus_attack = 0
			
		step: 1
	}
	theon_greyjoy: {
		-- Base
		id: 'xy47tepf',
		type: 'iijqtm761',
		name: 'Теон Грейджой',
		icon: 'assets/cards/norden/Teon.jpg',
		ztags: { '1jqza214' },
		support: true,
		
		-- Ingame
		attack: 1, health: 9
	}
	tyrion: {
		-- Base
		id: '6o1uelzc',
		type: 'iijqtm761',
		name: 'Tyrion',
		icon: 'assets/cards/regius/Tyrion.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 5, health: 16
	}
	tywin: {
		-- Base
		id: '4hkbing9',
		type: 'iijqtm761',
		name: 'Tywin',
		icon: 'assets/cards/regius/Tywin.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 12, health: 17
	}
	varys: {
		-- Base
		id: 'fnphrv7h',
		type: 'iijqtm761',
		name: 'Varys',
		icon: 'assets/cards/regius/Varys.jpg',
		ztags: { 'fa9442c6' },
		support: false,
		
		-- Ingame
		attack: 4, health: 19
	}
}

for _, card in pairs cards
	meta = {
		__index: (property) =>
			if property == 'tags' then fromArray(rawget(self, 'ztags'))
			else rawget(self, property)
	}
	setmetatable(card, meta)

return { :events, :cards }