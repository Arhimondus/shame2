local be = require 'bidons'
local flux = require 'flux'
local timer = require 'timer'

local dialog = {}

function dialog:on_start(scene)
	scene:get('backimage').background_image = nil
	scene:get('backimage').image = nil
	
	scene:get('default_dialog').sprites = {}
	scene:get('sprites'):updateBinding()
end

return dialog